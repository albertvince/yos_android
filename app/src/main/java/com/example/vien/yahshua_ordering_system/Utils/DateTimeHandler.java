package com.example.vien.yahshua_ordering_system.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateTimeHandler {

    public static String getTimeStamp(){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
            String format = simpleDateFormat.format(new Date());
            return format.replace("-","");

        } catch (Exception err) {
            return "";
        }
    }

    public static Date getDueDate(Date date, int daysInterval){
        try {
            final Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, daysInterval);

            return c.getTime();

        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDatetoDisplayDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String formatDateToDisplay(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }


    public static String convertDatetoStringDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss a");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String getTimeForError(Date date){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-dd-yyyy   h:mm:ss a");
            return simpleDateFormat.format(date);

        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDisplayDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String convert24to12(String time)
    {
        try
        {
            DateFormat originalFormat = new SimpleDateFormat("HH:mm");
            Date dateTime = originalFormat.parse(time);

            return formatTime(dateTime);

        } catch (Exception err)
        {
            return null;
        }
    }

    public static String formatTime(Date dateTIme)
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        return timeFormat.format(dateTIme);
    }

    public static Date convertStringtoDateForError(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("MMM-dd-yyyy   h:mma");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringtoDate(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringtoDate2(String strDate){
        try {
            DateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date addDaysToDate(Date date, int daysCount)
    {
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, daysCount);
        return c.getTime();
    }
}
