package com.example.vien.yahshua_ordering_system.DialogFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.example.vien.yahshua_ordering_system.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

import static android.content.Intent.FLAG_ACTIVITY_NO_HISTORY;

public class LoginDialog extends DialogFragment {

    // Context
    private Context context;
    private View view;

    //Widgets
    private Button btnLogin;
    private SwipeRefreshLayout swipeLayout;
    private EditText mPasswordView;
    private TextView tvName, tvEmail;
    private LinearLayout linearLayout;
    private ProgressBar progressBar;

    //Data
    private String email, getEmail, getFirstName, getLastName, getToken;
    JSONObject jsonParams;
    private UserSession userSession = new UserSession();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragment_login, container, false);
        context = view.getContext();

        if (getArguments() != null)
        {
            getEmail = getArguments().getString("EMAIL");
            getFirstName = getArguments().getString("FIRST_NAME");
            getLastName = getArguments().getString("LAST_NAME");
            getToken = getArguments().getString("TOKEN");
        }

        initializeUI();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);
    }

    private void initializeUI()
    {
        tvName = view.findViewById(R.id.tv_name);
        tvEmail = view.findViewById(R.id.tv_email);
        btnLogin = view.findViewById(R.id.btn_login);
        mPasswordView = view.findViewById(R.id.et_password);
        linearLayout = view.findViewById(R.id._layout);
        progressBar = view.findViewById(R.id._progressBar);

//        swipeLayout = view.findViewById(R.id.swipe_container);

        tvEmail.setText(getEmail);
        tvName.setText(getFirstName +" "+ getLastName);
        email = tvEmail.getText().toString();
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    attemptLogin();
                } catch (Exception err){
                    Toasty.error(context, err.toString()).show();
                }
            }
        });
    }

    public void attemptLogin()
    {
        try {
            // Reset errors.
            mPasswordView.setError(null);

            // Store values at the time of the login attempt.
            String password = mPasswordView.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordView;
                cancel = true;
            }

            if (TextUtils.isEmpty(password)) {
                mPasswordView.setError(getString(R.string.error_field_required));
                focusView = mPasswordView;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.

                getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                jsonParams = new JSONObject();

                jsonParams.put("username", email);
                jsonParams.put("password", password);

                Debugger.printO(jsonParams);

                StringEntity entity = new StringEntity(jsonParams.toString());

                if (!Utility.haveNetworkConnection(context)) {
                    mPasswordView.setError("Internet connection is required");
                    mPasswordView.requestFocus();

                } else {
                    hideKeyboard();
                    LoginYOS(entity);
                }
            }

            } catch(Exception err)
            {
                Toasty.error(context, err.toString()).show();
            }

    }

    public void hideKeyboard()
    {
        getDialog().setCancelable(false);
        InputMethodManager imm = (InputMethodManager)mPasswordView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 7;
    }

    // Pass data to server when log in
    public void LoginYOS(StringEntity entity) throws JSONException {

//        HttpClientProvider.
        HttpClientProvider.post(context,"login_mobile/",entity,"application/json", new JsonHttpResponseHandler()
        {

            @Override
            public void onStart() {
                super.onStart();
                showProgressBar(true);
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, org.json.JSONObject response)
            {
                try
                {
                    Toasty.success(context, "You are now logged in", Toast.LENGTH_SHORT).show();

                    userSession.user.setEmail(email);
                    userSession.setAuthToken(getToken);
                    userSession.user.setFirstName(getFirstName);
                    userSession.user.setLastName(getLastName);
                    userSession.saveUserSession(context);

                    Intent main_intent = new Intent(context, MainActivity.class);
                    main_intent.setFlags(FLAG_ACTIVITY_NO_HISTORY);
                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(main_intent);
                    getDialog().dismiss();
                    getActivity().finish();

                    showProgressBar(false);
                } catch (Exception err) {
                    Toasty.error(context, err.toString()).show();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable)
            {
                super.onFailure(statusCode, headers, responseString, throwable);
                try {
                    mPasswordView.setError(responseString);
                    mPasswordView.requestFocus();
                    showProgressBar(false);
                } catch (Exception err){
                    Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse ) {
                try {
                    mPasswordView.setError("Network is unreachable");
                    mPasswordView.requestFocus();
                    showProgressBar(false);
                } catch (Exception err){
                    Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure ( int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse ) {
                try {
                    mPasswordView.setError(errorResponse.toString());
                    mPasswordView.requestFocus();
                    showProgressBar(false);
                } catch (Exception err){
                    Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRetry ( int retryNo ) {
                Toasty.warning(context, "Retrying").show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                showProgressBar(false);
            }
        });
    }

    private void showProgressBar(boolean show){

        getDialog().setCancelable(!show);
        linearLayout.setVisibility( show ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }
    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

}
