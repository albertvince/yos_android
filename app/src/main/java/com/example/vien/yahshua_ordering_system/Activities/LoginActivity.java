package com.example.vien.yahshua_ordering_system.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.Loader;
import android.database.Cursor;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Adapter.UserAccountAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.example.vien.yahshua_ordering_system.Utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */

    // Context and Views
    private Context context;
    private View mProgressView;
    private View mLoginFormView;

    // Widgets
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private Button signUp,mEmailSignInButton;
    private CheckBox cbRememberMe;
    private TextView tvGotoAccount, tvStatus, tvForgotPassword;

    // Data
    private static final int REQUEST_READ_CONTACTS = 0;
    private String email;
    JSONObject jsonParams;
    private ArrayList<UserAccount> userAccountArrayList = new ArrayList<>();
    private UserAccountAdapter userAccountAdapter;
    private UserAccount selectedUserAccount;
    private UserSession userSession = new UserSession();
    private boolean cbChecked;
    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;

        initializeUI();
    }

    // Initialize component from a layout file
    private void initializeUI(){

        signUp = findViewById(R.id.btnSignUp);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        cbRememberMe = findViewById(R.id.cb_remember_me);
        tvGotoAccount = findViewById(R.id.tv_goto_account);
        tvStatus = findViewById(R.id.tv_status);
        tvForgotPassword = findViewById(R.id.tv_forgot_password);

        userAccountAdapter = new UserAccountAdapter(context, R.layout.listrow_account, userAccountArrayList);


        tvGotoAccount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context,UserAccountActivity.class);
                startActivity(intent);
                finish();
            }
        });

        cbRememberMe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cbChecked = cbRememberMe.isChecked();
            }
        });

        mEmailView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    CharSequence searchText = "";
//                    searchText = s;
//                    readRecords(searchText.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        signUp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, SignupActivity.class);
                startActivity(intent);
            }
        });

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        tvForgotPassword.setPaintFlags(tvForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvForgotPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        readAccounts();
    }

    private void readRecords(String searchValue){

        try {
            String searchQuery = null;
            if (searchValue != null) searchQuery = " WHERE e_mail LIKE '%" + searchValue + "%' ";

            userAccountArrayList = UserAccount.read(context, searchQuery);
            userAccountAdapter.clear();
            userAccountAdapter.addAll(userAccountArrayList);
            userAccountAdapter.notifyDataSetChanged();

            if (userAccountArrayList.size() > 0 && searchValue.contains(".") || userAccountArrayList.size() == 1  && searchValue.contains("@")) {
                selectedUserAccount = userAccountAdapter.getItem(0);
                confirmAccount(selectedUserAccount.getEmail());
            }

        } catch (Exception err) {
            Debugger.printO(err);
        }
    }

    private void readAccounts(){

        userAccountArrayList = UserAccount.read(context, "");

        if(userAccountArrayList.size() > 0){
            tvGotoAccount.setVisibility(View.VISIBLE);
        }else{
            tvGotoAccount.setVisibility(View.GONE);
        }

    }

    private void confirmAccount(String email)
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                switch (which)
                {
                    case DialogInterface.BUTTON_POSITIVE:

                        userSession.setEmail(selectedUserAccount.getEmail());
                        userSession.setAuthToken(selectedUserAccount.getToken());
                        userSession.saveUserSession(context);

                        Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(main_intent);
                        finish();

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        PopUpProvider.buildConfirmAccountDialog(context, dialogClickListener,   email +" ?");
    }

    // Pass data to server when log in
    public void LoginYOS(StringEntity entity) throws JSONException {

//        HttpClientProvider.
        HttpClientProvider.post(getApplicationContext(),"login_mobile/",entity,"application/json", new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, org.json.JSONObject response)
            {
                try
                {
                    Toasty.success(getApplicationContext(), "You are now logged in",Toast.LENGTH_SHORT).show();
                    UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>(){}.getType());
                    Debugger.logD(response.toString());
                    session.saveUserSession(context);
                    if (session.saveUserSession(context))
                    {
                        finish();
                        Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(main_intent);
                    }

                    if(cbChecked)
                    {
                        UserAccount userAccount = new UserAccount();
                        userAccount.setToken(UserSession.getToken(context));
                        userAccount.setEmail(UserSession.getEmployeeEmail(context));
                        userAccount.setFirstName(UserSession.getFirstName(context));
                        userAccount.setLastName(UserSession.getLastName(context));
                        userAccount.setYpoEmployeeId(UserSession.getYpoEmployeeId(context));
                        userAccount.save(context);
                    }

                } catch (Exception err) {
                    Toasty.error(context, err.toString()).show();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable)
            {
                super.onFailure(statusCode, headers, responseString, throwable);
                try {
                    mPasswordView.setError(responseString);
                    mPasswordView.requestFocus();
                    showProgress(false);
                } catch (Exception err){
                    Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse ) {
                try {
                    mPasswordView.setError("Network is unreachable");
                    mPasswordView.requestFocus();
                    showProgress(false);
                } catch (Exception err){
                    Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure ( int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse ) {
                try {
                    mPasswordView.setError(errorResponse.toString());
                    mPasswordView.requestFocus();
                    showProgress(false);
                } catch (Exception err){
                    Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRetry ( int retryNo ) {
                Toasty.warning(context, "Retrying").show();
            }
        });
    }

    private void attemptLogin()
    {
        try
        {
            // Reset errors.
            mEmailView.setError(null);
            mPasswordView.setError(null);

            // Store values at the time of the login attempt.
            email = mEmailView.getText().toString();
            String password = mPasswordView.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordView;
                cancel = true;
            }

            if (TextUtils.isEmpty(password)) {
                mPasswordView.setError(getString(R.string.error_field_required));
                focusView = mPasswordView;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.

                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                jsonParams = new JSONObject();

                jsonParams.put("username", email);
                jsonParams.put("password", password);

                Debugger.printO(jsonParams);

                StringEntity entity = new StringEntity(jsonParams.toString());

                if (!Utility.haveNetworkConnection(context)) {
                    mEmailView.setError("Internet connection is required");
                    mEmailView.requestFocus();
                } else {
                   hideKeyBoard();
                   LoginYOS(entity);
                   showProgress(true);
                }
            }

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    //Hide Keyboard when submit credentials
    private void hideKeyBoard(){
        try
        {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                @Override
                @TargetApi(Build.VERSION_CODES.M)
                public void onClick(View v) {
                    requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                }
            });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 7;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            tvStatus.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    tvStatus.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            tvStatus.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

