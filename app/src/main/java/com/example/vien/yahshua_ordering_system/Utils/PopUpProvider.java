package com.example.vien.yahshua_ordering_system.Utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.widget.ImageView;

import com.example.vien.yahshua_ordering_system.R;

public class PopUpProvider {

    public static void buildYesNoDialog(Context context,String title, String message, DialogInterface.OnClickListener dialogInterface)
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setIcon(R.drawable.ic_android_delete);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Yes", dialogInterface);
        builder.setNegativeButton("No", dialogInterface);
        builder.show();
    }

    public static void buildConfirmationDialog(Context context, DialogInterface.OnClickListener dialogInterface,String title,String messsage)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(messsage).setPositiveButton("Yes", dialogInterface)
                .setNegativeButton("No", dialogInterface).show();
    }

    public static void buildConfirmAccountDialog(Context context, DialogInterface.OnClickListener dialogInterface,String messsage)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Is this your account ");
        builder.setMessage((Html.fromHtml("<font color='#7CAE3B'>"+ messsage +"</font>"))).setPositiveButton("Yes", dialogInterface)
                .setNegativeButton("No", dialogInterface).show();
    }

    public static void buildCheckoutConfirmDialog(Context context, DialogInterface.OnClickListener dialogInterface,String title,String messsage)
    {
        ImageView image = new ImageView(context);
        image.setImageResource(R.drawable.ic_android_checkmark_circle);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.drawable.ic_android_checkmark_circle);
        builder.setTitle(title);
        builder.setMessage(messsage).setPositiveButton("Done", dialogInterface).show();
    }


}