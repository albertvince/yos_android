package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vien.yahshua_ordering_system.Adapter.AccountViewPagerAdapter;
import com.example.vien.yahshua_ordering_system.Model.Company;
import com.example.vien.yahshua_ordering_system.Model.CompanyType;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import androidx.navigation.Navigation;

public class CompanyTypeFragment extends Fragment {

    // Views and Context
    private View view;
    private Context context;

    //Widget
    private TabLayout tabLayout;
    private AccountViewPagerAdapter accountViewPagerAdapter;
    private ViewPager viewPager;

    //Data
    private static final String COMPANYTYPE = "arg1";
    private static final String COMPANYNAME = "arg2";
    private static final String COMPANYUUID = "arg3";
    private String companyName, companyUuid;
    private CompanyType companyType;

    public CompanyTypeFragment(){

    }

    public static CompanyTypeFragment newInstance(CompanyType param1, String param2, String param3) {
        CompanyTypeFragment fragment = new CompanyTypeFragment();
        Bundle args = new Bundle();
        args.putParcelable(COMPANYTYPE, param1);
        args.putString(COMPANYNAME, param2);
        args.putString(COMPANYUUID, param3);
        fragment.setArguments(args);
        return fragment;
    }

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_company_type, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();

        if (getArguments() != null) {
            companyType = getArguments().getParcelable(COMPANYTYPE);
            companyName = getArguments().getString(COMPANYNAME);
            companyUuid = getArguments().getString(COMPANYUUID);
        }

        initializeUI(view);
    }

    private void initializeUI(final View view){

        Debugger.logD("companyType");
        tabLayout = view.findViewById(R.id.tab_company);
        viewPager = view.findViewById(R.id.view_pager_company);
//
//        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                Debugger.logD(tab.getPosition() +" position");
//                if(tab.getPosition() == 0 ){
//
//                }else if(tab.getPosition() == 1){
//                    CompanyTypeFragmentDirections.ActionCompanyTypeFragmentToStoreProductFragment action;
//                    action = new CompanyTypeFragmentDirections.ActionCompanyTypeFragmentToStoreProductFragment();
//                    Navigation.findNavController(view).navigate(action);
//                }
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        if(companyType != null) {
            readViewPager(false,null, companyType);
            getActivity().setTitle(companyType.getName());
        } else{
            readViewPager(true, companyUuid, null);
            tabLayout.setVisibility(View.GONE);
            getActivity().setTitle(companyName);
        }
    }

    private void readViewPager(boolean show, String uuid, CompanyType companyType){

        accountViewPagerAdapter = new AccountViewPagerAdapter(getChildFragmentManager());

        if(show){
//            accountViewPagerAdapter.addFragment(StoreProductFragment.newInstance(uuid), "Food");
//            CompanyTypeFragmentDirections.ActionCompanyTypeFragmentToStoreProductFragment action;
//            action = new CompanyTypeFragmentDirections.ActionCompanyTypeFragmentToStoreProductFragment();
//            Navigation.findNavController(view).navigate(action);
        } else{
//            accountViewPagerAdapter.addFragment( CompanyFragment.newInstance(companyType) , "Store");
//            accountViewPagerAdapter.addFragment((StoreProductFragment.newInstance(companyUuid, companyName)), "Food");
        }

        viewPager.setAdapter(accountViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

}
