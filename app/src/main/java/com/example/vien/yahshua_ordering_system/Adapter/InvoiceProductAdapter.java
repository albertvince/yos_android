package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Model.InvoiceProduct;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Converter;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InvoiceProductAdapter extends ArrayAdapter<InvoiceProduct> {

    private ArrayList<InvoiceProduct> orderedDetailsArrayList;
    private TextView tvName, tvQuantity, tvPrice, tvTotal, tvVoid;
    private ImageView imageView;

    public InvoiceProductAdapter(Context context, ArrayList<InvoiceProduct> arrayList) {
        super(context, 0, arrayList);
        orderedDetailsArrayList = arrayList;
    }

    @Override
    public int getPosition(@Nullable InvoiceProduct item)
    {
        int x = 0;
        return super.getPosition(item);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        InvoiceProduct invoiceProduct = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_bought_item, parent, false);
        }

        tvName     = convertView.findViewById(R.id.tvBoughtItemName);
        tvQuantity = convertView.findViewById(R.id.tvBoughtQuantity);
        tvTotal    = convertView.findViewById(R.id.tvBoughtTotalPrice);
        imageView  = convertView.findViewById(R.id.ivImage);
        tvPrice    = convertView.findViewById(R.id.tvBoughtPrice);
        tvVoid     = convertView.findViewById(R.id.tv_void);

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        DecimalFormat formatQuantity = new DecimalFormat("#,###");
        String priceLabel = formatter.format(invoiceProduct.getPrice());
        String totalGrossLabel = formatter.format(invoiceProduct.getTotalGross());
        String quantityLabel = formatQuantity.format(invoiceProduct.getQuantity());

        tvName.setText(invoiceProduct.getInventory().getName());
        tvQuantity.setText(String.valueOf(quantityLabel));
        tvTotal.setText(String.format("₱%s",totalGrossLabel));
        tvPrice.setText(String.format("₱%s",addZeroes(priceLabel)));

        if(invoiceProduct.isVoid()){
            tvVoid.setText(String.valueOf(Converter.BooleanIsVoid(true)));
        }else if(invoiceProduct.isConfirmed()){
            tvVoid.setText(String.valueOf(Converter.BooleanIsConfirmed(true)));
        } else {
            tvVoid.setText("");
        }

        ifImageAvailable(invoiceProduct);

        return convertView;
    }

    /* Display list of image using Glide */
    private void displayImage(InvoiceProduct obj)
    {
        RequestOptions myOption = new RequestOptions()
                .centerInside();

        Glide.with(getContext())
                .load(obj.getInventory().getImageURL().replace("//media","/media"))
                .apply(myOption)
                .into(imageView);

    }

    private void ifImageAvailable(InvoiceProduct invoiceProduct){

        if (invoiceProduct.getInventory().getImageURL().equals(HttpClientProvider.getBaseURL() + "media/default_inventory.jpg") || !invoiceProduct.getInventory().getImageURL().contains("/media")) {
            Debugger.logD("Default image");
        } else {
            displayImage(invoiceProduct);
        }
    }

    private String addZeroes(String val){
        if (val.equals(".00")) val = "00" + val;
        return val;
    }

}
