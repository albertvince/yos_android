package com.example.vien.yahshua_ordering_system.Model;

import com.google.gson.annotations.SerializedName;

public class Category  {

    private String code;
    private String name;
    @SerializedName("uuid")
    private String uuId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }
}
