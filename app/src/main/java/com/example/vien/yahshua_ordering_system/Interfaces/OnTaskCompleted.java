package com.example.vien.yahshua_ordering_system.Interfaces;


import android.os.Bundle;

public interface OnTaskCompleted {
    void onTaskCompleted();

    interface onFilterDialogCompleteListener{
        void onFilterDone(Bundle bundle);
    }
}
