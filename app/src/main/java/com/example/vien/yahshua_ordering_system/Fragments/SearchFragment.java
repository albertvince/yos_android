package com.example.vien.yahshua_ordering_system.Fragments;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.R;

public class SearchFragment extends Fragment {

    // Views and Context
    private View view;
    private Context context;

    //Widgets
    private Button btnProduct, btnStore;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();

        initializeUI();
    }

    // Initialize component from a layout file
    private void initializeUI() {

        btnProduct = view.findViewById(R.id.btn_product);
        btnStore = view.findViewById(R.id.btn_store);

        ((MainActivity) context).tabLayout.setVisibility(View.GONE);

        btnProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).displayTabForHomeFragment();
            }
        });

        btnStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).replaceFragment(CompanyFragment.class, null );
            }
        });
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }
}
