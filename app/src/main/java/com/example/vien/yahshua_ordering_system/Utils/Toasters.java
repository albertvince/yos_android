package com.example.vien.yahshua_ordering_system.Utils;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.R;

public final class Toasters {

    public static ProgressDialog progress;

    public Toasters(Context context)
    {
        progress = new ProgressDialog(context);
        progress.setIndeterminate(false);
        progress.setMax(100);
        progress.setMessage("Loading...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIcon(R.drawable.bug_icon);
        progress.setCancelable(false);
        progress.show();
    }

    public void setMaxProgress(int max)
    {
        progress.setMax(max);
    }

    public void updateToaster(String message, int test)
    {
        progress.setMessage(message);
        progress.setProgress(test);

    }

    public void stopToaster()
    {
        progress.dismiss();
    }

    public static void ShowToast(Context v, String message) {
        Toast toast = Toast.makeText(v, "" + message, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void ShowLoadingSpinner(final Context context, boolean cancelable) {
        try {
            progress = new ProgressDialog(context);
            progress.setMessage("Loading...");
            progress.setCancelable(cancelable); // disable dismiss by tapping outside of the dialog
            progress.show();
        } catch (Exception err) {

        }
    }

    public static void ShowProgressSpinner(final Context context, String message, boolean cancelable)
    {
        try
        {
            progress = new ProgressDialog(context);
            progress.setMessage(message);
            progress.setCancelable(cancelable); // disable dismiss by tapping outside of the dialog
            progress.show();
        } catch (Exception er) {

        }
    }

    public static void HideLoadingSpinner() {
        progress.dismiss();
    }

    public static void ShowSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    static void sendNotification(Context context, int id, String contentTitle, String contentText) {
        try {
//            Intent createSessionIntent = new Intent(context, CreateStudentSession.class);

//            PendingIntent pendingIntent = PendingIntent.getActivity(context, id, createSessionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
            final NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                            .setContentTitle(contentTitle)
//                            .setContentIntent(pendingIntent)
                            .setContentText(contentText);

            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(id, mBuilder.build());
        } catch (Exception err) {
            Toasters.ShowToast(context, err.toString());
        }
    }


}