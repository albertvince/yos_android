package com.example.vien.yahshua_ordering_system.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.BuildConfig;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

public class AccountFragment extends Fragment {

    // View and Context
    private View view;
    private Context context;

    // Widget
    private EditText tvFirstName, tvLastName, tvCode, tvEmail, tvEmployeeId;
    private ImageView imageView;
    private ProgressDialog progressDialog;
    private Button btnUpdate, btnUpload;
    private boolean updateSuccess = false;

    //Data
    private ArrayList<UserAccount> userAccountArrayList = new ArrayList<>();

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();
        initializeUI();
    }

    // Initialize component from a layout file
    private void initializeUI() {
        tvFirstName = view.findViewById(R.id.tv_firstname);
        tvLastName = view.findViewById(R.id.tv_lastname);
        tvCode = view.findViewById(R.id.tv_code);
        tvEmail = view.findViewById(R.id.tv_email);
        tvEmployeeId = view.findViewById(R.id.tv_employee_id);
        imageView = view.findViewById(R.id.iv_User);
        btnUpdate = view.findViewById(R.id.btn_update);
        btnUpload = view.findViewById(R.id.btn_UploadImage);

        ((MainActivity) context).tabLayout.setVisibility(View.GONE);
        getUserProfile();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserProfile();
            }
        });

        //showProfilePic();

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

    }

    private void dispatchTakePictureIntent() {
        int permissionCheck = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            requestRuntimePermission();
        }
        //REQUEST PERMISSION
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 555);
            } catch (Exception err) {
                Debugger.logD(err.toString());
                Toasty.error(context, err.toString()).show();
            }
        } else {
            Debugger.logD("Camera Clicked");
            pickImage();
        }
    }

    private void requestRuntimePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
            Toast.makeText(context, "Need camera feature to use this app.", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 555 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage();
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 555);
        }
    }

    public void pickImage() {
        Debugger.logD("Pick Image");
        CropImage.startPickImageActivity(getActivity());
    }

    private void croprequest(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //RESULT FROM SELECTED IMAGE
        super.onActivityResult(requestCode, resultCode, data);
        Debugger.logD("IMAGE SELECTED");
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(context, data);

            croprequest(imageUri);
        }

        //RESULT FROM CROPING ACTIVITY
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());

                    imageView.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showProfilePic() {
        if (UserSession.getProfilePicture(context).contains("media")) {
            RequestOptions myOption = new RequestOptions().circleCrop();
            Glide.with(context).load(UserSession.getProfilePicture(context)).apply(myOption).into(imageView);
        }
    }

    private void displayEmployeeDetails() {

        tvFirstName.setText(UserSession.getFirstName(getContext()));
        tvLastName.setText(UserSession.getLastName(getContext()));
        tvCode.setText(UserSession.getEmployeeCode(getContext()));
        tvEmail.setText(UserSession.getEmployeeEmail(getContext()));
        tvEmployeeId.setText(UserSession.getYpoEmployeeId(getContext()));
//        showProfilePic();
    }

    public void getUserProfile() {
        try {

            HttpClientProvider.get(context, "customer_profile/", null, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>() {
                        }.getType());
                        session.saveUserSession2(context);
                        if (session.saveUserSession2(context)) {
                            displayEmployeeDetails();
                            ((MainActivity) getActivity()).updateNavName(context);
                        }

                        if (updateSuccess) {

                            Toasty.success(context, "Update success", Toast.LENGTH_SHORT).show();

                            //Update account in database
                            String searchQuery;
                            searchQuery = " WHERE e_mail LIKE '%" + UserSession.getEmployeeEmail(context) + "%' ";
                            userAccountArrayList = UserAccount.read(context, searchQuery);

                            for (UserAccount userAccount : userAccountArrayList) {
                                userAccount.setFirstName(UserSession.getFirstName(getContext()));
                                userAccount.setLastName(UserSession.getLastName(getContext()));
                                userAccount.setEmail(UserSession.getEmployeeEmail(getContext()));
                                userAccount.setToken(UserSession.getToken(getContext()));
                                userAccount.save(context);
                            }

                        }

                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Debugger.printO("JSONObject " + throwable.toString());
                    Toasty.error(context, "Server Error").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.error(context, responseString).show();
                }

                @Override
                public void onFinish() {
                    progressDialog.dismiss();
                }
            });


        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }

    }

    private void updateUserProfile() {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("first_name", tvFirstName.getText().toString());
            jsonObject.put("last_name", tvLastName.getText().toString());
            jsonObject.put("code", tvCode.getText().toString());
            jsonObject.put("ypo_employee_id", tvEmployeeId.getText().toString());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "customer_profile/", stringEntity, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        super.onSuccess(statusCode, headers, response);

                        getUserProfile();
                        updateSuccess = true;

                    } catch (Exception err) {
//                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, "Server Error").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    Toasty.error(context, errorResponse.toString()).show();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
//                    Toasty.error(context, responseString).show();
                }

                @Override
                public void onFinish() {
                    progressDialog.dismiss();
                }

            });

        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }
}
