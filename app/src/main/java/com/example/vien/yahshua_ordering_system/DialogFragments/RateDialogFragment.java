package com.example.vien.yahshua_ordering_system.DialogFragments;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.R;

public class RateDialogFragment extends DialogFragment
{
    private Context context;
    private View view;
    private RatingBar ratingBar;
    private TextView ratingValue;
    private Button btnSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_frag_rate, container, false);
        context = view.getContext();

        initiaizeUI();
        return view;
    }

    private void initiaizeUI()
    {
        ratingValue = view.findViewById(R.id.rating);
        btnSubmit = view.findViewById(R.id.submit);
        ratingBar = view.findViewById(R.id.RatingBar);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingValue.setText("" + rating);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        setDialogOnTop();

        super.onResume();
    }

    private void setDialogOnTop()
    {
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.x = 200;
        getDialog().getWindow().setAttributes(p);
    }

    public int getTheme()
    {
        return R.style.full_screen_dialog;
    }
}
