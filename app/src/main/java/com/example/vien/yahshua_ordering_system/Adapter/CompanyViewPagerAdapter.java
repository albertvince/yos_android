package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Model.Category;
import com.example.vien.yahshua_ordering_system.R;

import java.util.ArrayList;

public class CompanyViewPagerAdapter extends PagerAdapter{

    private LayoutInflater layoutInflater;
    private ArrayList<Category> mData;
    private Fragment fragment;

    public CompanyViewPagerAdapter(Context context,Fragment fragment, ArrayList<Category> data){
        this.layoutInflater = LayoutInflater.from(context);
        this.mData = data;
        this.fragment = fragment;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mData.size();
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = layoutInflater.inflate(R.layout.company_header_images, null);
        TextView textView = view.findViewById(R.id.tv_name);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar_Header);
        final Category category = mData.get(position);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("rating", category.getUuId());
                bundle.putString("name", category.getName());
//                Intent details = new Intent(context, DetailsActivity.class);
//                details.putExtras(bundle);
//                context.startActivity(details);
            }
        });

        textView.setText(category.getName());
//        ratingBar.setRating(company.getUuId() +"");

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

}
