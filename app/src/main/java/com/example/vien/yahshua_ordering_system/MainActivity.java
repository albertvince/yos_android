package com.example.vien.yahshua_ordering_system;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Activities.AccountActivity;
import com.example.vien.yahshua_ordering_system.Activities.LoginActivity;
import com.example.vien.yahshua_ordering_system.Activities.PurchaseActivity;
import com.example.vien.yahshua_ordering_system.Activities.UserAccountActivity;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Fragments.AccountFragment;
import com.example.vien.yahshua_ordering_system.Fragments.CompanyFragment;
import com.example.vien.yahshua_ordering_system.Fragments.DailySalesFragment;
import com.example.vien.yahshua_ordering_system.Fragments.HomeFragment;
import com.example.vien.yahshua_ordering_system.Fragments.NewHomeFragment;
import com.example.vien.yahshua_ordering_system.Fragments.OrderedFragment;
import com.example.vien.yahshua_ordering_system.Fragments.SearchFragment;
import com.example.vien.yahshua_ordering_system.Fragments.ShopFragment;
import com.example.vien.yahshua_ordering_system.Model.Category;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // Views and Context
    private View nav_header_view;
    private Context context;

    // Widgets
    private TextView tvCompanyName, tvCompanyEmail, tvName;
    private ImageView imageView;
    private ViewPager mViewPager;
    public TabLayout tabLayout;
    private NavigationView navigationView;
    private DrawerLayout drawer;

    // Data
    private FragmentManager manager;
    private int selectNavId;
    private ArrayList<Category> categoryArrayList = new ArrayList<>();
    private String uuId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        setTitle("Home");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.container);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                switch (selectNavId) {

//                    case R.id.nav_search:
//                        openSearchFragment();
//                        break;
//                    case R.id.nav_home:
//                        displayTabForHomeFragment();
                    case R.id.nav_home:
                        openDashboardFragment();
                        break;
                    case R.id.nav_shope:
                        openShopFragment();
                        break;
                    case R.id.nav_daily_sales:
                        openDailySalesFragment();
                        break;
//                    case R.id.nav_store:
//                        openCompanyFragment();
//                        break;
//                    case R.id.nav_purchase:
//                        openPurchaseActivity();
//                        openOrderedFragment();
//                        break;
                    case R.id.nav_account:
//                        openAccountFragment();
                        openAccountActivity();
                        break;
                    case R.id.nav_logout:
                        logoutSession();
                }
            }

        };

        // hide built-in Title
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//
//        // Setting background using a drawable
//        Drawable toolbarBackground = getResources().getDrawable(R.drawable.ic_menu_gallery);
//        getSupportActionBar().setBackgroundDrawable(toolbarBackground);

        //Hides the hamburger icon and disable its click event
        toggle.setDrawerIndicatorEnabled(false);

        //Hamburger icon replacement
//        toggle.setHomeAsUpIndicator(R.drawable.ic_person_black_24dp);

        //Add click event to the new icon
//        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                drawer.openDrawer(GravityCompat.START);
//            }
//        });
//

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        loadToolbarDetails();
        initializeUI();
        checkUserSession();
    }

    private void loadToolbarDetails() {
        ImageView ivToolbar = findViewById(R.id.iv_ToolbarProfilePic);
        if (UserSession.getLocalImage(context).equals("")) {
            ivToolbar.setImageResource(R.drawable.ic_toolbar_profile_pic);
        } else {
            RequestOptions myOption = new RequestOptions().circleCrop();
            Glide.with(context).load(UserSession.getLocalImage(context)).apply(myOption).into(ivToolbar);
        }
        ivToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        TextView tvToolbar = findViewById(R.id.tv_ToolbarFullName);
        tvToolbar.setText(UserSession.getFirstName(context) + " " + UserSession.getLastName(context));
    }

    private void initializeUI() {

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        nav_header_view = navigationView.getHeaderView(0);

        tvName = (TextView) nav_header_view.findViewById(R.id.tv_name);
        tvCompanyName = (TextView) nav_header_view.findViewById(R.id.tvCompanyName);
        tvCompanyEmail = (TextView) nav_header_view.findViewById(R.id.tvEmail);
        imageView = (ImageView) nav_header_view.findViewById(R.id.iv_profile_picture);


    }

    @Override
    public void onResume() {
        super.onResume();
//        refreshFragment();
//        openDailySalesFragment();
        openDashboardFragment();
        updateNavName(context);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.content_main);

            if (f instanceof NewHomeFragment) {
                closeSession();
            } else {
//                openHomeFragment();
//                openDailySalesFragment();
                openDashboardFragment();
            }

        }
    }

    private void checkUserSession() {
        String authToken = UserSession.getToken(this);

        if (authToken.length() <= 0 && UserAccount.checkUserCount(context) > 0) {
            openUserActivity();
        } else if (authToken.length() <= 0) {
            Intent login_intent = new Intent(this, LoginActivity.class);
            startActivity(login_intent);
            this.finish();
        } else {
            updateNavName(context);
//            openDailySalesFragment();
            openDashboardFragment();
        }
    }

    public void updateNavName(Context context) {
        tvName.setText(UserSession.getFirstName(context) + " " + UserSession.getLastName(context));
        tvCompanyName.setText(UserSession.getCompany(this));
        tvCompanyEmail.setText(UserSession.getEmployeeEmail(this));
        showProfilePic();
    }

    public void showProfilePic() {
//        if (UserSession.getProfilePicture(context).contains("media")) {
//            RequestOptions myOption = new RequestOptions().circleCrop();
//            Glide.with(context).load(UserSession.getProfilePicture(context)).apply(myOption).into(imageView);
//        }
        if (UserSession.getLocalImage(context).equals("")) {
            imageView.setImageResource(R.drawable.account_profile);
        } else {
            RequestOptions myOption = new RequestOptions().circleCrop();
            Glide.with(context).load(UserSession.getLocalImage(context)).apply(myOption).into(imageView);
        }

    }

    private void logoutSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        UserSession.clearSession(context);
                        removeAllInCart();
                        checkUserSession();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm logout", "Are you sure you want to log out?");
    }

    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        MainActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Are you sure you want to close application?", null);
    }

    private void removeAllInCart() {
        Checkout checkout = new Checkout();
        checkout.deleteAll(context);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        selectNavId = id;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void refreshFragment() {
        try {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.content_main);

            if (f instanceof CompanyFragment) {
                openCompanyFragment();
            } else if (f instanceof OrderedFragment) {
                openOrderedFragment();
            } else if (f instanceof AccountFragment) {
//                openAccountFragment();
            } else {
//                openHomeFragment();
                openDailySalesFragment();
            }


        } catch (Exception err) {

            Toasty.error(context, err.toString()).show();
        }
    }

    private void openShopFragment() {

        ShopFragment shopFragment = new ShopFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, shopFragment, shopFragment.getTag()).commit();
    }

    private void openPurchaseActivity() {
        setTitle("My Purchase");
        Intent intent = new Intent();
        intent.setClass(context, PurchaseActivity.class);
        startActivity(intent);
    }

    private void openDashboardFragment() {
        setTitle("Home");
//        DashBoardFragment dashBoardFragment = new DashBoardFragment();
//        manager = getSupportFragmentManager();
//        manager.beginTransaction().replace(R.id.content_main, dashBoardFragment, dashBoardFragment.getTag()).commit();
        NewHomeFragment newHomeFragment = new NewHomeFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, newHomeFragment, newHomeFragment.getTag()).commit();
    }

    private void openUserActivity() {
        Intent intent = new Intent();
        intent.setClass(context, UserAccountActivity.class);
        startActivity(intent);
        this.finish();
    }

    private void openHomeFragment(String uuid) {

        Bundle bundle = new Bundle();
        bundle.putString("uuid", uuid);

        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setArguments(bundle);
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, homeFragment, homeFragment.getTag()).commit();
    }

    private void openOrderedFragment() {
        setTitle("My Purchases");
        OrderedFragment orderedFragment = new OrderedFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, orderedFragment, orderedFragment.getTag()).commit();
    }

    private void openAccountFragment() {
        setTitle("Account");
        AccountFragment accountFragment = new AccountFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, accountFragment, "ACCOUNT_TAG").commit();
    }

    private void openAccountActivity() {
        Intent account = new Intent(MainActivity.this, AccountActivity.class);
        startActivity(account);

    }

    private void openCompanyFragment() {
        setTitle("Store");
        CompanyFragment companyFragment = new CompanyFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, companyFragment, companyFragment.getTag()).commit();
    }

    private void openDailySalesFragment() {
        navigationView.setCheckedItem(R.id.nav_daily_sales);
        setTitle("Daily Sales");
        DailySalesFragment dailySalesFragment = new DailySalesFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, dailySalesFragment, dailySalesFragment.getTag()).commit();
    }

    private void openSearchFragment() {
        setTitle("Select Category");
        replaceFragment(SearchFragment.class, null);
    }

    public void replaceFragment(Class fragmentClass, Bundle bundle) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
    }

    // Online functions
    public void fetchCategory() {
        try {
            HttpClientProvider.get(context, "read_category/", null, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        tabLayout.removeAllTabs();

                        categoryArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Category>>() {
                        }.getType());

                        Category category = new Category();
                        category.setName("All");
                        categoryArrayList.add(0, category);

                        for (Category inventory : categoryArrayList) {
                            tabLayout.addTab(tabLayout.newTab().setText(inventory.getName()));
                        }

                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toasty.error(context, "Can't Connect to Server").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.error(context, responseString).show();
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                }

                @Override
                public void onFinish() {
                }
            });

        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }
    }

    public void displayTabForHomeFragment() {
        fetchCategory();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                for (int i = 0; i < categoryArrayList.size(); i++) {
                    if (position > 0) {
                        uuId = categoryArrayList.get(position).getUuId();
                        Bundle bundle = new Bundle();
                        bundle.putString("uuid", uuId);
                        openHomeFragment(uuId);
                        break;
                    } else {
                        openHomeFragment("");
                        break;
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public void setNavPic(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        Debugger.logD("ACTIVITY RESULT:");
        //RESULT FROM SELECTED IMAGE
        Debugger.logD("IMAGE SELECTED");
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(context, data);

            croprequest(imageUri);
        }

        //RESULT FROM CROPING ACTIVITY
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), result.getUri());

                    imageView.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void croprequest(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }
}
