package com.example.vien.yahshua_ordering_system.Adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class AccountViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mLabelList = new ArrayList<>();

    public AccountViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return mFragmentList.get(i);
    }

    @Override
    public int getCount() {
        return mLabelList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mLabelList.get(position);
    }

    public void addFragment(Fragment fragment, String label) {
        mFragmentList.add(fragment);
        mLabelList.add(label);
    }
}
