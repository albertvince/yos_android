package com.example.vien.yahshua_ordering_system.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Activities.OrderDetailsActivity;
import com.example.vien.yahshua_ordering_system.Adapter.HistoryRecyclerViewAdapter;
import com.example.vien.yahshua_ordering_system.Adapter.InvoiceAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Interfaces.RecyclerViewClick;
import com.example.vien.yahshua_ordering_system.Model.Invoice;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.navigation.Navigation;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment{

    // Views and Context
    private View view;
    private Context context;

    // Adapter
    private HistoryRecyclerViewAdapter historyRecyclerViewAdapter;

    // Widget
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private View viewEmptyListIndicator;
    private TextView tvTotal;

    // ArrayList & Data
    private ArrayList<Invoice> invoiceArrayList = new ArrayList<>();

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_history, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        context = view.getContext();
        initializeUI();
        fetchCustomerInvoices("", null, null);
    }

    private void initializeUI() {
        tvTotal = view.findViewById(R.id.tv_HistoryTotal);
        recyclerView = view.findViewById(R.id.recyclerView_History);

    }

    // Online functions
    private void fetchCustomerInvoices(String search, String dateFrom, String dateTo) {
        try {
            RequestParams params = new RequestParams();
            params.put("from_mobile", true);
            params.put("search_text", search);
            params.put("date_from", dateFrom);
            params.put("date_to", dateTo);

            HttpClientProvider.get(context, "read_customer_invoices/", params, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        progressDialog.hide();
                        invoiceArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Invoice>>() {
                        }.getType());
                        readRecords();
                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, "Can't connect to server").show();
                    progressDialog.hide();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    Toasty.error(context, "Can't connect to server").show();
                    progressDialog.hide();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toasty.error(context, " " + responseString).show();
                    progressDialog.hide();
                }

                @Override
                public void onFinish() {
                    progressDialog.hide();
                }

            });
        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }
    }

    private void readRecords() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        historyRecyclerViewAdapter = new HistoryRecyclerViewAdapter(context, invoiceArrayList, new RecyclerViewClick() {
            @Override
            public void onItemClick(View v, int position) {
                Toasty.info(context, invoiceArrayList.get(position).getReferenceNo()).show();

                Intent intent = new Intent(view.getContext(), OrderDetailsActivity.class);
                intent.putExtra("INVOICE", invoiceArrayList.get(position));
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(historyRecyclerViewAdapter);
        totalAmountPurchase();
    }

    private void totalAmountPurchase() {

        int totalAmount = 0;

        DecimalFormat formatter = new DecimalFormat("#,###.00");

        for (Invoice invoice : invoiceArrayList) {
            totalAmount += invoice.getTotalGross();
        }

        String amountLabel = formatter.format(totalAmount);
        tvTotal.setText("₱" + String.valueOf(addZeroes(amountLabel)));
    }

    private String addZeroes(String val) {
        if (val.equals(".00")) val = "0" + val;
        return val;
    }

}
