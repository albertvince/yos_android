package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Activities.CheckoutActivity;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Fragments.CheckOutFragment;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;

import java.text.DecimalFormat;
import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CheckoutAdapter extends ArrayAdapter<Checkout> {

    private TextView price, name, quantity;
    private ArrayList<Checkout> checkoutArrayList;
    private ImageView imageView;
    private Button btnCancel, btnMinus,btnPlus;
    private Fragment fragment;

 public CheckoutAdapter(Context context,Fragment fragment, @LayoutRes int resource, ArrayList<Checkout> arrayList) {
     super(context, resource, arrayList);
     this.checkoutArrayList = arrayList;
     this.fragment = fragment;
 }

    public int getSubTotal() {
        int total = 0;
        for (Checkout checkout : this.checkoutArrayList) {
            total += checkout.getItemPrice() * checkout.getItemQuantity();
        }
        return total;
    }

    @Override
    public int getPosition(@Nullable Checkout item)
        {
        int x = 0;
        for(Checkout checkout : checkoutArrayList)
        {
            if(checkout.getId() == checkout.getId())
            {
                return x;
            }else{
                x++;
            }
        }

        return super.getPosition(item);
        }

    @Override
    public @NonNull
    View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Checkout checkout = getItem(position);

        if (convertView == null) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_selected_item, parent, false);
        }

        DecimalFormat formatQuantity= new DecimalFormat("#,###");
        String totalQuantity = formatQuantity.format(checkout.getItemQuantity());

        price = convertView.findViewById(R.id.tvSelectedItemPrice);
        name = convertView.findViewById(R.id.tvSelectedItemName);
        imageView = convertView.findViewById(R.id.ivItemImage);
        btnCancel = convertView.findViewById(R.id.btnCancel);
        btnMinus = convertView.findViewById(R.id.btnDeductQuantity);
        btnPlus = convertView.findViewById(R.id.btnAddQuantity);
        quantity = convertView.findViewById(R.id.tvQuantity);

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (checkout.getItemQuantity() <= 0) {
                        Toasty.warning(getContext(), "Zero quantity", Toast.LENGTH_SHORT).show();
                    } else {
                        checkout.addQuantity(-1);
                        notifyDataSetChanged();
//                        ((CheckoutActivity) getContext()).refreshTotal();
                    }

            }

        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkout.addQuantity(1);
                notifyDataSetChanged();
//                ((CheckoutActivity) getContext()).refreshTotal();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmCancel(checkout);

            }
        });

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String priceLabel = formatter.format(checkout.getItemPrice());

        quantity.setText(totalQuantity);
        price.setText(String.format("₱%s",addZeroes(priceLabel)));
        name.setText(checkout.getItemName());
        quantity.setText(String.valueOf(checkout.getItemQuantity()));

        ifImageAvailable(checkout);
        return convertView;
        }

    private void confirmCancel(final Checkout checkout)
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        cancelCheckout(checkout);
                        Toasty.success(getContext(), "Order cancelled", Toast.LENGTH_SHORT).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        PopUpProvider.buildYesNoDialog(getContext(),"Cancel","Are you sure you want to cancel your order "+checkout.getItemName()  + " ?", dialogClickListener);
    }

    private void cancelCheckout(Checkout checkout)
    {
        checkout.delete(getContext());
        remove(checkout);
        notifyDataSetChanged();
        ((CheckOutFragment) fragment).refreshTotal();
    }

    //Display list of image using Glide
    private void displayImage(Checkout obj)
    {
        RequestOptions myOption = new RequestOptions()
                .centerInside();

        Glide.with(getContext())
                .load(obj.getImageURL().replace("//media","/media"))
                .apply(myOption)
                .into(imageView);

    }

    private void ifImageAvailable(Checkout checkout){

        if (checkout.getImageURL().equals(HttpClientProvider.getBaseURL() + "media/default_inventory.jpg") || !checkout.getImageURL().contains("/media")) {
            Debugger.logD("Default image");
        } else {
            displayImage(checkout);
        }
    }

    private String addZeroes(String val){
        if (val.equals(".00")) val = "00" + val;
        return val;
    }


}