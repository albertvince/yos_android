package com.example.vien.yahshua_ordering_system.DialogFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Adapter.ContactsRecyclerViewAdapter;
import com.example.vien.yahshua_ordering_system.Interfaces.RecyclerViewClick;
import com.example.vien.yahshua_ordering_system.Model.ContactsModel;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.KeyboardHandler;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class PayDialog extends DialogFragment {

    private Context context;
    private View view;

    private String amountLabel;
    private boolean isEmail = false;

    private TabLayout tabLayout;
    private ConstraintLayout constraintLayoutPay, constraintLayoutContacts;
    private RecyclerView recyclerView;
    private TextView tvBalance, tvEmailAccNo;
    private EditText etEmailAccNo, etAmount, etSearch;
    private Button btnDone;

    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private Cursor cursor;
    private ContactsRecyclerViewAdapter contactsRecyclerViewAdapter;
    private ArrayList<ContactsModel> contacts = new ArrayList<>();


    public static PayDialog newInstance(String balance) {
        PayDialog frag = new PayDialog();
        Bundle args = new Bundle();
        args.putString("BALANCE", balance);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_wallet_pay, container);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        context = getContext();

        tabLayout = view.findViewById(R.id.tabLayout_WalletPay);
        constraintLayoutPay = view.findViewById(R.id.constraint_WalletPay);
        constraintLayoutContacts = view.findViewById(R.id.constraint_WalletContacts);
        recyclerView = view.findViewById(R.id.recyclerView_WalletContacts);
        tvBalance = view.findViewById(R.id.tv_PayBalance);
        tvEmailAccNo = view.findViewById(R.id.tv_PayEmailAccNo);
        etEmailAccNo = view.findViewById(R.id.et_PayEmailAccNo);
        etAmount = view.findViewById(R.id.et_PayAmount);
        etSearch = view.findViewById(R.id.et_ContactSearch);
        btnDone = view.findViewById(R.id.btn_PayDone);

        tvEmailAccNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmail) {
                    etEmailAccNo.setHint("Email");
                    etEmailAccNo.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    etEmailAccNo.setText("");
                    tvEmailAccNo.setText("Use Account No?");
                    isEmail = false;
                } else {
                    etEmailAccNo.setHint("Account No");
                    etEmailAccNo.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etEmailAccNo.setText("");
                    tvEmailAccNo.setText("Use Email?");
                    isEmail = true;
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                contactsRecyclerViewAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Toasty.success(context, "Sent.").show();
            }
        });

        amountLabel = getArguments().getString("BALANCE", "0.00");
        tvBalance.setText("₱" + amountLabel);
        addTabs();
        permissionCheck();

    }

    private void addTabs() {
        tabLayout.removeAllTabs();
        tabLayout.addTab(tabLayout.newTab().setText("Pay"));
        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    constraintLayoutPay.setVisibility(View.VISIBLE);
                    constraintLayoutContacts.setVisibility(View.GONE);
                    KeyboardHandler.closeKeyboard(view, context);
                } else {
                    constraintLayoutPay.setVisibility(View.GONE);
                    constraintLayoutContacts.setVisibility(View.VISIBLE);
                    KeyboardHandler.closeKeyboard(view, context);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void permissionCheck() {
        int permission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            loadContacts();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts();
            } else {
                Toasty.warning(context, "Until you grant the permission, we can't display the contacts.").show();
            }
        }
    }

    private void loadContacts() {
        cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC ");

        contacts.clear();
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String image = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

            contacts.add(new ContactsModel(image, name, number));
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        contactsRecyclerViewAdapter = new ContactsRecyclerViewAdapter(context, contacts);
        contactsRecyclerViewAdapter.setOnClick(new RecyclerViewClick() {
            @Override
            public void onItemClick(View v, int position) {
//                Toast.makeText(context, contacts.get(position).getFullName(), Toast.LENGTH_SHORT).show();
                openPromptDialog(contacts.get(position));
            }
        });
        recyclerView.setAdapter(contactsRecyclerViewAdapter);
    }

    private void openPromptDialog(ContactsModel contactsModel) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_wallet_prompt, null);
        final TextView tvName = dialogView.findViewById(R.id.tv_WalletPromtFullName);
        final Button btnOK = dialogView.findViewById(R.id.btn_WalletPromptOK);
        final Button btnCancel = dialogView.findViewById(R.id.btn_WalletPromptCancel);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        tvName.setText(contactsModel.getFullName());

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                b.dismiss();
                Toasty.success(context, "OK clicked.").show();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        b.show();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onResume() {
//        Window window = getDialog().getWindow();
////        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        window.setGravity(Gravity.CENTER);

        // Store access variables for window and blank point
        Window window = getDialog().getWindow();
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        // Set the height of the dialog proportional to 75% of the screen width
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, (int) (size.x * 0.95));
        window.setGravity(Gravity.CENTER);
        // Call super onResume after sizing
        super.onResume();
    }
}
