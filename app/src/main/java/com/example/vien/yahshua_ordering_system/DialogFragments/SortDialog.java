package com.example.vien.yahshua_ordering_system.DialogFragments;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.example.vien.yahshua_ordering_system.Fragments.HomeFragment;
import com.example.vien.yahshua_ordering_system.Fragments.ProductsFragment;
import com.example.vien.yahshua_ordering_system.Fragments.StoreProductFragment;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

public class SortDialog extends DialogFragment {
    // Context
    private Context context;
    private View view;

    // Widget
    private Button btnDone;
    private RadioButton rbHighPrice, rbLowPrice;

    //Data
    private Fragment fragment;
    private String sortType, list;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragment_sort, container, false);
        context = view.getContext();

        if (getArguments() != null)
        {
            list = getArguments().getString("LIST");
        }

        initializeUI();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);
    }

    private void initializeUI()
    {
        btnDone = view.findViewById(R.id.btnDone);
        rbHighPrice = view.findViewById(R.id.rd_high_price);
        rbLowPrice = view.findViewById(R.id.rd_low_price);

        rbHighPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortType = "-price";

            }
        });

        rbLowPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortType = "price";

            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        try
        {
            Bundle bundle = new Bundle();
            bundle.putString("SORT_PRICE", sortType);

            switch (list)
            {
                case "HOME_PRODUCT" : ((HomeFragment)getTargetFragment()).onFilterDone(bundle); break;
                case "STORE_PRODUCT" : ((StoreProductFragment)getTargetFragment()).onFilterDone(bundle); break;
                case "DAILY_SALES" :  ((ProductsFragment)getTargetFragment()).onFilterDone(bundle);  break;
            }

        } catch (Exception err) {
            Debugger.logD("onDismiss "+err.toString());
        }
    }

    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}
