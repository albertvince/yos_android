package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.SearchView;


import com.example.vien.yahshua_ordering_system.Activities.CheckoutActivity;
import com.example.vien.yahshua_ordering_system.Adapter.HomeRecyclerViewAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.DialogFragments.FilterDialog;
import com.example.vien.yahshua_ordering_system.DialogFragments.SortDialog;
import com.example.vien.yahshua_ordering_system.DialogFragments.ViewItemsDialog;
import com.example.vien.yahshua_ordering_system.Interfaces.OnTaskCompleted;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.Model.Inventory;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.navigation.Navigation;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import es.dmoral.toasty.Toasty;

import static android.content.Intent.FLAG_ACTIVITY_NO_HISTORY;

public class HomeFragment extends Fragment  implements HomeRecyclerViewAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener, OnTaskCompleted.onFilterDialogCompleteListener{

    // Views and Context
    private View view;
    private  Context context;

    // Adapter
    HomeRecyclerViewAdapter homeRecyclerViewAdapter;

    // Widgets
    private Button filter, sort;
    private CardView cardView;
    private TextView textCartItemCount, tvItemQuantity, tvSubTotal;
    private SwipeRefreshLayout swipeLayout;
    private View viewEmptyListIndicator;
    private RecyclerView rcHomeProduct;

    // ArrayList and Data
    private ArrayList<Inventory> inventoryArrayList = new ArrayList<>();
    private Inventory selectedItem = new Inventory();
    private int count = 0;
    private Date today = new Date();
    private String subTotalLabel, sortResult, uuId;
    private ArrayList<Checkout> checkoutArrayList = new ArrayList<>();

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();
        getActivity().setTitle("Products");

        initializeUI();
        showConnectionDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        count = Checkout.getTotalItemCount(context);
        displaySubtotal();
        countItem();

    }

    // Override Function to Change Options Menu When this fragment is called
    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);

        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        final MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        final MenuItem searchViewItem = menu.findItem(R.id.action_search);


        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);
        View actView = quantityItem.getActionView();
        tvItemQuantity = (TextView) actView.findViewById(R.id.quantity_badge);


        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCheckout();
            }
        });

        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchSaleInventory(query, null,uuId);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchSaleInventory(newText, null,uuId);
                return true;
            }

        });
        super.onCreateOptionsMenu(menu, inflater);
        onResume();
        if (count == 0) {
            textCartItemCount.setVisibility(View.GONE);
        } else {
            onPause();
            textCartItemCount.setVisibility(View.VISIBLE);
            textCartItemCount.setText(count+"");
        }
    }

    //Viewing of item count in action bar
    public void countItem() {

        count = Checkout.getTotalItemCount(context);
        if (textCartItemCount != null) {
            if (count == 0) {
                textCartItemCount.setVisibility(View.GONE);
                subTotal();
                cardView.setVisibility(View.GONE);

            } else {
                textCartItemCount.setText(String.valueOf(count));
                textCartItemCount.setVisibility(View.VISIBLE);
                subTotal();
                cardView.setVisibility(View.VISIBLE);
            }
        }

    }

    // Initialize component from a layout file
    private void initializeUI() {

        filter = view.findViewById(R.id.btnFilter);
        sort = view.findViewById(R.id.btnSort);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        cardView = view.findViewById(R.id.cvSubtotal);
        viewEmptyListIndicator = view.findViewById(R.id.viewEmptyListIndicator);
        rcHomeProduct = view.findViewById(R.id.rc_home_product);

        ((MainActivity) context).tabLayout.setVisibility(View.VISIBLE);

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDialog filterDialog = new FilterDialog();
                filterDialog.show(getActivity().getFragmentManager(), "filter dialog");
            }
        });

        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSortDialog();
            }
        });

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        displaySubtotal();
    }

    private void openSortDialog(){

        Bundle bundle = new Bundle();
        bundle.putString("LIST", "HOME_PRODUCT");
        SortDialog sortDialog = new SortDialog();
        sortDialog.setArguments(bundle);
        sortDialog.setTargetFragment(HomeFragment.this, 100);
        sortDialog.show(getFragmentManager(), "ITEM");
    }

    // Read data from database
    private void readRecords() {
        rcHomeProduct.setLayoutManager(new GridLayoutManager(context, 2));
        homeRecyclerViewAdapter = new HomeRecyclerViewAdapter(context, HomeFragment.this, inventoryArrayList);
        homeRecyclerViewAdapter.setClickListener(this);
        rcHomeProduct.setAdapter(homeRecyclerViewAdapter);
        registerForContextMenu(rcHomeProduct);

        showEmptyView(inventoryArrayList.size() < 1);
    }

    private void showEmptyView(boolean show)
    {
        viewEmptyListIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        rcHomeProduct.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public void addItemToCart(int position){

        selectedItem = homeRecyclerViewAdapter.getItem(position);
        checkoutArrayList = Checkout.readForCheckout(context, selectedItem.getUuId());

        if(checkoutArrayList.size() != 0){
        for(Checkout checkout : checkoutArrayList) {
            checkout.setItemQuantity(selectedItem.getQuantity());
                checkout.setDate(today);
                checkout.setUuId(selectedItem.getUuId());
                checkout.save(context);
            }
        }else {
                for (Inventory inventory : inventoryArrayList) {
                    if (inventory.getUuId().equals(selectedItem.getUuId())) {
                        Checkout checkout = new Checkout();
                        checkout.setItemName(selectedItem.getName());
                        checkout.setItemPrice(selectedItem.getPrice());
                        checkout.setItemQuantity(selectedItem.getQuantity());
                        checkout.setItemImage(selectedItem.getItemImage());
                        checkout.setDate(today);
                        checkout.setUuId(selectedItem.getUuId());
                        checkout.save(context);
                        break;
                    }
                }
        }
    }

    private void goToCheckout() {
        Intent intent = new Intent();

            if (count != 0) {
                intent.setFlags(FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setClass(context, CheckoutActivity.class);
                startActivity(intent);
            } else {
                Toasty.warning(context, "No item selected.", Toast.LENGTH_SHORT).show();
            }
    }

    private void subTotal() {

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        subTotalLabel = formatter.format( Checkout.getTotalOrder(context));
        tvSubTotal.setText(String.valueOf("₱" + subTotalLabel));
    }

    private void displaySubtotal(){

        if (count == 0) {
            subTotal();
            cardView.setVisibility(View.GONE);
        } else {
            cardView.setVisibility(View.VISIBLE);
            subTotal();
        }
    }

    @Override
    public void onFilterDone(Bundle bundle) {

        sortResult = bundle.getString("SORT_PRICE");
        fetchSaleInventory("", sortResult,uuId);
    }

    // Online functions
    public void fetchSaleInventory(String search, String sortBy, String uuId)
    {
        try
        {
            RequestParams params = new RequestParams();
            params.put("from_mobile", true);
            params.put("search_text", search);
            params.put("sort_by", sortBy);
            params.put("category_uuid", uuId);

            HttpClientProvider.get(context, "read_sale_inventory/", params, new JsonHttpResponseHandler()
            {
                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
//                    swipeLayout.requestFocus();
                }

                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        swipeLayout.setRefreshing(false);
                        inventoryArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Inventory>>(){}.getType());
                        readRecords();
                    } catch (Exception err)
                    {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toasty.error(context, "Can't Connect to Server").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.error(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onFinish() {
                    swipeLayout.setRefreshing(false);
                }
            });


        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    private void showConnectionDialog(){

        if (!Utility.haveNetworkConnection(context)) {
            Utility.showNoConnectionDialog(context);
            swipeLayout.setRefreshing(false);
        } else {
            if(getArguments() != null){
                uuId = getArguments().getString("uuid");
                fetchSaleInventory("", null, uuId);
            } else{
                fetchSaleInventory("", null, "");
            }
        }
    }

    @Override
    public void onRefresh() {
        showConnectionDialog();
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        }

    @Override
    public void onItemClick(View view, final int position) {
        selectedItem = homeRecyclerViewAdapter.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putString("itemName", selectedItem.getName());
        bundle.putString("itemImage", selectedItem.getImageURL());
        bundle.putString("itemDescription", selectedItem.getDescription());
        bundle.putDouble("itemPrice", selectedItem.getPrice());
        bundle.putInt("itemStock", selectedItem.getQuantityLeft());

        ViewItemsDialog viewItemsDialog = new ViewItemsDialog();
        viewItemsDialog.setArguments(bundle);
        viewItemsDialog.show(getActivity().getFragmentManager(), "");
    }
}
