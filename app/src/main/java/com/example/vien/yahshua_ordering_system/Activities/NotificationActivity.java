package com.example.vien.yahshua_ordering_system.Activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.vien.yahshua_ordering_system.R;

public class NotificationActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        context = this;
        setTitle("Notification");
        getSupportActionBar().setElevation(0);
    }
}
