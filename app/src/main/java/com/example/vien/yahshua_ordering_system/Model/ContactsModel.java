package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ContactsModel implements Parcelable {

    private String image;
    private String fullName;
    private String number;

    public ContactsModel(String image, String fullName, String number) {
        this.image = image;
        this.fullName = fullName;
        this.number = number;
    }

    protected ContactsModel(Parcel in) {
        image = in.readString();
        fullName = in.readString();
        number = in.readString();
    }

    public static final Creator<ContactsModel> CREATOR = new Creator<ContactsModel>() {
        @Override
        public ContactsModel createFromParcel(Parcel in) {
            return new ContactsModel(in);
        }

        @Override
        public ContactsModel[] newArray(int size) {
            return new ContactsModel[size];
        }
    };

    public String getImage() {
        return image;
    }

    public String getFullName() {
        return fullName;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(fullName);
        dest.writeString(number);
    }
}
