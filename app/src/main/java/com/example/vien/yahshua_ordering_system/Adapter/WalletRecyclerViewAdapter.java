package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Interfaces.RecyclerViewClick;
import com.example.vien.yahshua_ordering_system.Model.WalletModel;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class WalletRecyclerViewAdapter extends RecyclerView.Adapter<WalletRecyclerViewAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<WalletModel> walletModelArrayList;

    private RecyclerViewClick listener;

    public WalletRecyclerViewAdapter(Context context, ArrayList<WalletModel> walletModelArrayList, RecyclerViewClick listener) {
        this.context = context;
        this.walletModelArrayList = walletModelArrayList;
        this.listener = listener;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.listrow_wallet, viewGroup, false);
        final MyViewHolder myViewHolder = new MyViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, myViewHolder.getAdapterPosition());
            }
        });
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        WalletModel walletModel = walletModelArrayList.get(i);

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String amountLabel = formatter.format(walletModel.getAmount());

        if (walletModel.getTransactionType().equals("Received")) {
            myViewHolder.ivType.setImageResource(R.drawable.ic_wallet_received);
            myViewHolder.tvType.setText(walletModel.getTransactionType());
            myViewHolder.tvDate.setText(DateTimeHandler.convertDisplayDate(walletModel.getDate()));
            myViewHolder.tvTime.setText(DateTimeHandler.formatTime(walletModel.getDate()));
            myViewHolder.tvAmount.setText(String.valueOf("₱" + amountLabel));
            if (walletModel.userSource.getFullName().equals(" ")) {
                myViewHolder.tvFullName.setText(walletModel.userSource.getEmail());
            } else {
                myViewHolder.tvFullName.setText(walletModel.userSource.getFullName());
            }
        } else {
            myViewHolder.ivType.setImageResource(R.drawable.ic_wallet_sent);
            myViewHolder.tvType.setText(walletModel.getTransactionType());
            myViewHolder.tvDate.setText(DateTimeHandler.convertDisplayDate(walletModel.getDate()));
            myViewHolder.tvTime.setText(DateTimeHandler.formatTime(walletModel.getDate()));
            myViewHolder.tvAmount.setText(String.valueOf("₱" + amountLabel));
            if (walletModel.userDestination.getFullName().equals(" ")) {
                myViewHolder.tvFullName.setText(walletModel.userDestination.getEmail());
            } else {
                myViewHolder.tvFullName.setText(walletModel.userDestination.getFullName());
            }
        }
    }

    @Override
    public int getItemCount() {
        return walletModelArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivType;
        private TextView tvType, tvFullName, tvTime, tvDate, tvAmount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivType = itemView.findViewById(R.id.iv_WalletType);
            tvType = itemView.findViewById(R.id.tv_WalletType);
            tvFullName = itemView.findViewById(R.id.tv_WalletFullName);
            tvTime = itemView.findViewById(R.id.tv_WalletTime);
            tvDate = itemView.findViewById(R.id.tv_WalletDate);
            tvAmount = itemView.findViewById(R.id.tv_WalletAmount);
        }
    }


}
