package com.example.vien.yahshua_ordering_system.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Adapter.AccountViewPagerAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Fragments.HistoryFragment;
import com.example.vien.yahshua_ordering_system.Fragments.ProfileFragment;
import com.example.vien.yahshua_ordering_system.Fragments.WalletFragment;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.KeyboardHandler;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class AccountActivity extends AppCompatActivity {

    private Context context;
    private View view;

    private ImageView ivProfile;
    private TextView tvFullName, tvWallet;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog progressDialog;

    private AccountViewPagerAdapter accountViewPagerAdapter;
    private String newImagePath;
    private Bitmap bitmap;

    private String amountLabel = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        context = this;
        setTitle("Account");
        getSupportActionBar().setElevation(0);

        initializeUI();
        getWalletBalance();
        showProfile();
        populateViewPager();
    }

    private void initializeUI() {
        ivProfile = findViewById(R.id.iv_AccountImage);
        tvFullName = findViewById(R.id.tv_AccountFullname);
        tvWallet = findViewById(R.id.tv_AccountWallet);
        tabLayout = findViewById(R.id.tabLayout_Account);
        viewPager = findViewById(R.id.viewPager_Account);

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, ivProfile);
                popupMenu.getMenuInflater().inflate(R.menu.account_image_popup_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Change")) {
                            dispatchTakePictureIntent();
                        } else {
                            removeImage();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getWalletBalance() {

        try {
            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("invoice", invoice.getUuid());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "get_wallet_balance/", stringEntity, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        progressDialog.hide();
                        DecimalFormat formatter = new DecimalFormat("#,##0.00");
                        amountLabel = formatter.format(response.getInt("balance"));
                        tvWallet.setText("₱" + amountLabel);
                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    Toasty.error(context, "Can't connect to server").show();
                    progressDialog.hide();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    Toasty.error(context, "Can't connect to server").show();
                    progressDialog.hide();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
//                    Toasty.error(context, " " + responseString).show();
                    progressDialog.hide();
                }

                @Override
                public void onFinish() {
                    progressDialog.hide();
                }

            });
        } catch (Exception err) {
//            Toasty.error(context, err.toString()).show();
        }
    }

    private void populateViewPager() {
        //Add fragments
        accountViewPagerAdapter = new AccountViewPagerAdapter(getSupportFragmentManager());
        accountViewPagerAdapter.addFragment(new ProfileFragment(), "Profile");
        accountViewPagerAdapter.addFragment(new HistoryFragment(), "History");
        accountViewPagerAdapter.addFragment(new WalletFragment(), "Wallet");
        viewPager.setAdapter(accountViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void dispatchTakePictureIntent() {
        int permissionCheck = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            requestRuntimePermission();
        }
        //REQUEST PERMISSION
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 555);
            } catch (Exception err) {
                Debugger.logD(err.toString());
                Toasty.error(context, err.toString()).show();
            }
        } else {
            Debugger.logD("Camera Clicked");
            pickImage();
        }
    }

    private void requestRuntimePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            Toast.makeText(context, "Need camera feature to use this app.", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 3);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 555 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage();
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 555);
        }
    }

    public void pickImage() {
        Debugger.logD("Pick Image");
        CropImage.startPickImageActivity(this);
    }

    private void croprequest(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //RESULT FROM SELECTED IMAGE
        super.onActivityResult(requestCode, resultCode, data);
        Debugger.logD("IMAGE SELECTED");
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(context, data);

            croprequest(imageUri);
        }

        //RESULT FROM CROPING ACTIVITY
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
//                    ivEmpImage.setImageBitmap(bitmap);
                    getNewImagePath(bitmap);
                    RequestOptions myOption = new RequestOptions().circleCrop();
                    Glide.with(context).load(newImagePath).apply(myOption).into(ivProfile);
//                    ((MainActivity) context).updateNavName(context);
                    UserSession userSession = new UserSession();
                    userSession.setLocalImage(newImagePath);
                    userSession.saveLocalImage(context);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getNewImagePath(Bitmap bitmapImage) {
        ContextWrapper contextWrapper = new ContextWrapper(context);
        File fileDirectory = contextWrapper.getDir("my_images", Context.MODE_PRIVATE);
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaPath = new File(fileDirectory, "IMG_" + timeStamp + ".jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mediaPath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        Toasty.success(context,"IMG_" + timeStamp + ".jpg" + " saved").show();
        newImagePath = mediaPath.getAbsolutePath();
        return fileDirectory.getAbsolutePath();
    }

    public void showProfile() {

        tvFullName.setText(UserSession.getFirstName(context) + " " + UserSession.getLastName(context));
        if (UserSession.getLocalImage(context).equals("")) {
            ivProfile.setImageResource(R.drawable.account_profile);
        } else {
            RequestOptions myOption = new RequestOptions().circleCrop();
            Glide.with(context).load(UserSession.getLocalImage(context)).apply(myOption).into(ivProfile);
        }
    }

    private void removeImage() {
        UserSession userSession = new UserSession();
        userSession.removeLocalImage(context);
        showProfile();
    }

    public String getBalance() {
        return amountLabel;
    }
}
