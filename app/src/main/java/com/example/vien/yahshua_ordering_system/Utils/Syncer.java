package com.example.vien.yahshua_ordering_system.Utils;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Activities.PaySuccessActivity;
import com.example.vien.yahshua_ordering_system.Activities.PaymentMethodActivity;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Fragments.HomeFragment;
import com.example.vien.yahshua_ordering_system.Fragments.PaymentMethodFragment;
//import com.example.vien.yahshua_ordering_system.Fragments.PaymentMethodFragmentDirections;
import com.example.vien.yahshua_ordering_system.Fragments.PaymentMethodFragmentDirections;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.Model.Inventory;
import com.example.vien.yahshua_ordering_system.Model.InvoicePayment;
import com.example.vien.yahshua_ordering_system.Model.InvoiceProducts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;

import androidx.navigation.Navigation;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class Syncer
{
    private static ArrayList<Checkout> checkouts = new ArrayList<>();

    public static void saveSales(final Context context, final boolean inBackground, final View view, int personCount, String arrivalTime, String arrivalDate, String paymentType)
    {
        try
        {   int total = 0;
            final ArrayList<Checkout> checkoutArrayList = Checkout.read(context);
            final ArrayList<InvoiceProducts> invoiceProducts = InvoiceProducts.readForUpload(context);
            if (checkoutArrayList.size() == 0)
            {
                Toasty.error(context, "No sales to upload to server").show();
                return;
            }

            Checkout checkout = new Checkout();

            checkout.setPersonCount(personCount);
            checkout.setArrivalDate(arrivalDate);
            checkout.setArrivalTime(arrivalTime);
            checkout.setInvoiceProducts(invoiceProducts);

            InvoicePayment invoicePayment = new InvoicePayment();
            invoicePayment.setPaymentType(paymentType);
            if(paymentType.equals("CASH")) {
                invoicePayment.setAmountTendered(0);
            }
            checkout.setInvoicePayment(invoicePayment);

            StringEntity entity = new StringEntity(new Gson().toJson(checkout));

            Debugger.printO(checkout);

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {

                @Override
                public void onStart()
                {
                    if (!inBackground)
                    {
                        Toasters.ShowLoadingSpinner(context, false);
                    }
                }

                @Override
                public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    if (!inBackground)
                    {
                        Toasters.HideLoadingSpinner();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    Toasty.success(context, "Sales successfully uploaded").show();
                    checkouts = Checkout.read(context);

                    for(Checkout checkout : checkouts ) {
                        checkout.setCheckout(true);
                        checkout.save(context);
                    }

                    PaymentMethodFragmentDirections.ActionPaymentMethodFragmentToPaySuccessActivity action;
                    action = new PaymentMethodFragmentDirections.ActionPaymentMethodFragmentToPaySuccessActivity();
                    Navigation.findNavController(view).navigate(action);

//                    Intent intent = new Intent();
//                    intent.setClass(context, PaySuccessActivity.class);
//                    context.startActivity(intent);

//                    ((PaymentMethodActivity) context).finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        if (responseBody != null)
                        {
                            String responseString = new String(responseBody, "UTF-8");
                            Toasty.error(context, "Code: " + statusCode + "\nMessage: " + responseString).show();

                        }

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            };

            HttpClientProvider.postUpload(context, "save_sales/", entity, asyncHttpResponseHandler);

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }
}
