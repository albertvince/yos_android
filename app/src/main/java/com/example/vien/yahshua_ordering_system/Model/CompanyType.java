package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CompanyType  implements Parcelable {

    private String name;
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyType(){

    }

    protected CompanyType(Parcel in) {
        name = in.readString();
        uuid = in.readString();
    }

    public static final Creator<CompanyType> CREATOR = new Creator<CompanyType>() {
        @Override
        public CompanyType createFromParcel(Parcel in) {
            return new CompanyType(in);
        }

        @Override
        public CompanyType[] newArray(int size) {
            return new CompanyType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(uuid);
    }
}
