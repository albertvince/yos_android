package com.example.vien.yahshua_ordering_system.DialogFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.support.v4.app.DialogFragment;

import com.example.vien.yahshua_ordering_system.Classes.GlobalVariables;
import com.example.vien.yahshua_ordering_system.Fragments.HomeFragment;
import com.example.vien.yahshua_ordering_system.Fragments.OrderedFragment;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class FilterDateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    // Context
    private Context context;
    private View view;

    // Widget
    private Button btnDone;
    private EditText etDateTo, etDateFrom;

    //Data
    private boolean isDateFrom;
    private Date dateFrom,dateTo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragment_filter_date, container, false);
        context = view.getContext();

        initializeUI();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);
    }

    private void initializeUI()
    {
        btnDone = view.findViewById(R.id.btnDone);
        etDateTo = view.findViewById(R.id.etDateFrom);
        etDateFrom = view.findViewById(R.id.etDateTo);

        etDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
                isDateFrom = true;
            }
        });

        etDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
                isDateFrom = false;
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void openDatePicker()
    {
        DatePickers.DatePickerForFragment fragment = new DatePickers.DatePickerForFragment();
        fragment.setTargetFragment(this, 0);
        fragment.show(getFragmentManager(), "date");
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        try
        {
            Bundle bundle = new Bundle();

            bundle.putString("DATE_FROM", DateTimeHandler.convertDatetoStringDate(dateFrom));
            bundle.putString("DATE_TO", DateTimeHandler.convertDatetoStringDate(dateTo));

            ((OrderedFragment)getTargetFragment()).onFilterDone(bundle);

        } catch (Exception err) {
            Debugger.logD("onDismiss "+err.toString());
        }
    }

    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setDate(final Calendar calendar, View v)
    {
        try
        {
            if (isDateFrom)
            {
                dateFrom = calendar.getTime();
                etDateFrom.setText(DateTimeHandler.convertDatetoDisplayDate(calendar.getTime()));
            }else{
                dateTo = calendar.getTime();
                etDateTo.setText(DateTimeHandler.convertDatetoDisplayDate(calendar.getTime()));
            }

        } catch (Exception err){
            Toasty.error(v.getContext(), err.toString()).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
    {
        try
        {
            Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
            setDate(cal, view);
        } catch (Exception err){
            GlobalVariables.ShowToast(view.getContext(), err.toString());
        }
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}
