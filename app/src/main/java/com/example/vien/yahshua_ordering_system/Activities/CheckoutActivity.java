package com.example.vien.yahshua_ordering_system.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Adapter.CheckoutAdapter;
import com.example.vien.yahshua_ordering_system.Classes.GlobalVariables;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.Model.CompanyType;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CheckoutActivity extends AppCompatActivity {

    // Context
    private Context context;

    //Data

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        context =  this;

        setTitle("Shopping Cart");
        initializeUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    // Initialize component from a layout file
    private void initializeUI(){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
