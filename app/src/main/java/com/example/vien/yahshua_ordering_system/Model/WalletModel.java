package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class WalletModel implements Parcelable {

    @SerializedName("transaction_no")
    private String transactionNumber;
    private String remarks;
    private int amount;
    private Date date;
    @SerializedName("transaction_type")
    private String transactionType;

    @SerializedName("user_destination")
    public UserDestination userDestination = new UserDestination();
    public class UserDestination {
        private String uuid;
        private String code;
        @SerializedName("full_name")
        private String fullName;
        private String email;

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    @SerializedName("source_user")
    public UserSource userSource = new UserSource();
    public class UserSource {
        private String uuid;
        private String code;
        @SerializedName("full_name")
        private String fullName;
        private String email;

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public WalletModel() {

    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public UserDestination getUserDestination() {
        return userDestination;
    }

    public void setUserDestination(UserDestination userDestination) {
        this.userDestination = userDestination;
    }

    public UserSource getUserSource() {
        return userSource;
    }

    public void setUserSource(UserSource userSource) {
        this.userSource = userSource;
    }

    protected WalletModel(Parcel in) {
        amount = in.readInt();
        transactionType = in.readString();
    }

    public static final Creator<WalletModel> CREATOR = new Creator<WalletModel>() {
        @Override
        public WalletModel createFromParcel(Parcel in) {
            return new WalletModel(in);
        }

        @Override
        public WalletModel[] newArray(int size) {
            return new WalletModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(amount);
        dest.writeString(transactionType);
    }
}
