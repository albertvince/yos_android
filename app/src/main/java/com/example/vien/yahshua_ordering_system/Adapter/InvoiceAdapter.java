package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Model.Invoice;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InvoiceAdapter extends ArrayAdapter<Invoice> {

    private ArrayList<Invoice> invoiceArrayList;
    private TextView code, gross, date;

    public InvoiceAdapter(Context context, @LayoutRes int resource, ArrayList<Invoice> arrayList) {
        super(context, resource, arrayList);
        invoiceArrayList = arrayList;
        }

    @Override
    public int getPosition(@Nullable Invoice item) {
        int x = 0;
        for(Invoice invoice : invoiceArrayList)
        {
        if(invoice.getStatus() == item.getStatus())
        {
        return x;
        }else{
        x++;
        }
        }

        return super.getPosition(item);
        }

//    Sort to Desc
//    @Nullable
//    @Override
//    public Invoice getItem(int position) {
//        return super.getItem(getCount()-position-1);
//    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Invoice invoice = getItem(position);

        if (convertView == null) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_invoice, parent, false);
        }

        code = convertView.findViewById(R.id.tvCode);
        gross = convertView.findViewById(R.id.tvTotalGross);
        date = convertView.findViewById(R.id.tvDate);

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String amountLabel = formatter.format(invoice.getTotalGross());

        code.setText("Ref.: "+invoice.getReferenceNo());
        date.setText("Date: "  + DateTimeHandler.formatDateToDisplay(invoice.getDateCreated()));
        gross.setText(String.valueOf("₱"+amountLabel));

        return convertView;

    }
}