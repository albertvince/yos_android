package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Model.MapFields;
import com.example.vien.yahshua_ordering_system.R;

import java.util.ArrayList;

public class DashBoardResultAdapter extends RecyclerView.Adapter<DashBoardResultAdapter.ViewHolder> {

    // data is passed into the constructor
    private ArrayList<MapFields> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;


    public DashBoardResultAdapter(Context context, ArrayList<MapFields> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public DashBoardResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listrow_dashboard_result, parent, false);
        return new DashBoardResultAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DashBoardResultAdapter.ViewHolder holder, final int position) {
        final MapFields mapFields = mData.get(position);
        holder.tvName.setText(mapFields.getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<MapFields> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        CardView cardView;
        TextView tvName;

        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv_daily_sales);
            tvName = itemView.findViewById(R.id.tv_name);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    // convenience method for getting data at click position
    public MapFields getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}
