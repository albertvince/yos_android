package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;


import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Invoice implements Parcelable
{
    private int id;
    private String uuid;
    @SerializedName("reference_no")
    private String referenceNo;
    private String invoiceNo;
    private String receiptNo;
    private String status;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("total_net")
    private int totalNet;
    @SerializedName("total_gross")
    private int totalGross;
    @SerializedName("total_discount")
    private int totalDiscount;
    private String code;
    @SerializedName("date_created")
    private Date dateCreated;

    public Invoice() {

    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getTotalNet() {
        return totalNet;
    }

    public void setTotalNet(int totalNet) {
        this.totalNet = totalNet;
    }

    public int getTotalGross() {
        return totalGross;
    }

    public void setTotalGross(int totalGross) {
        this.totalGross = totalGross;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getId() {
        return id;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public String getReceiptNo() {
        return receiptNo;
    }



    public String getStatus() {
        return status;
    }


    public int getTotalDiscount() {
        return totalDiscount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }


    public void setStatus(String status) {
        this.status = status;
    }


    public void setTotalDiscount(int totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    protected Invoice(Parcel in) {
        id = in.readInt();
        uuid = in.readString();
        invoiceNo = in.readString();
        receiptNo = in.readString();
        status = in.readString();
        companyName = in.readString();
        totalDiscount = in.readInt();
    }

    public static final Creator<Invoice> CREATOR = new Creator<Invoice>() {
        @Override
        public Invoice createFromParcel(Parcel in) {
            return new Invoice(in);
        }

        @Override
        public Invoice[] newArray(int size) {
            return new Invoice[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(uuid);
        dest.writeString(invoiceNo);
        dest.writeString(receiptNo);
        dest.writeString(status);
        dest.writeString(companyName);
        dest.writeInt(totalDiscount);
    }
}
