package com.example.vien.yahshua_ordering_system.Model;

import com.google.gson.annotations.SerializedName;

public class InvoicePayment {

    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("amount_tendered")
    private double amountTendered;

    public double getAmountTendered() {
        return amountTendered;
    }

    public void setAmountTendered(double amountTendered) {
        this.amountTendered = amountTendered;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
