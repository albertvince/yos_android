package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Activities.OrderDetailsActivity;
import com.example.vien.yahshua_ordering_system.Adapter.InvoiceAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.DialogFragments.FilterDateDialog;
import com.example.vien.yahshua_ordering_system.Interfaces.OnTaskCompleted;
import com.example.vien.yahshua_ordering_system.Model.Invoice;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class OrderedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnTaskCompleted.onFilterDialogCompleteListener {
    // Views and Context
    private View view;
    private Context context;

    // Adapter
    private InvoiceAdapter invoiceAdapter;

    // Widget
    private ListView listView;
    private SwipeRefreshLayout swipeLayout;
    private View viewEmptyListIndicator;
    private Button btnFilter;
    private TextView tvTotal;

    // ArrayList & Data
    private ArrayList<Invoice> invoiceArrayList = new ArrayList<>();

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ordered, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = view.getContext();

        getActivity().setTitle("My Purchases");
        initializeUI(view);
        fetchCustomerInvoices("", null, null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.purchase_menu, menu);

        final MenuItem searchViewItem = menu.findItem(R.id.action_search);

        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchCustomerInvoices(query, null, null);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchCustomerInvoices(newText, null, null);

                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_filter) {
            openDateFilterDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Initialize component from a layout file
    private void initializeUI(View view) {

        listView = view.findViewById(R.id.lvOrder);
        viewEmptyListIndicator = view.findViewById(R.id.viewEmptyListIndicator);
        tvTotal = view.findViewById(R.id.tv_total);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Invoice invoice = invoiceAdapter.getItem(position);

//                OrderedFragmentDirections.ActionOrderedFragmentToOrderDetailsActivity action;
//                action = new OrderedFragmentDirections.ActionOrderedFragmentToOrderDetailsActivity(invoice);
//                action.setArg1(invoice);
//                Navigation.findNavController(view).navigate(action);

                Toasty.info(context, invoice.getReferenceNo()).show();

                Intent intent = new Intent(view.getContext(), OrderDetailsActivity.class);
                intent.putExtra("INVOICE", invoice);
                startActivity(intent);

            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getChildAt(0) != null) {
                    swipeLayout.setEnabled(listView.getFirstVisiblePosition() == 0 && listView.getChildAt(0).getTop() == 0);
                }
            }
        });

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        invoiceAdapter = new InvoiceAdapter(context, R.layout.listrow_invoice, invoiceArrayList);
        listView.setAdapter(invoiceAdapter);
    }

    // Read data from database
    private void readRecords() {
        invoiceAdapter.clear();
        invoiceAdapter.addAll(invoiceArrayList);
        invoiceAdapter.notifyDataSetChanged();
        totalAmountPurchase();

        showEmptyView(invoiceArrayList.size() < 1);
    }

    private void openDateFilterDialog() {
        FilterDateDialog filterDateDialog = new FilterDateDialog();
        filterDateDialog.setTargetFragment(this, 100);
        filterDateDialog.show(getFragmentManager(), "ITEM");
    }

    private void totalAmountPurchase() {

        int totalAmount = 0;

        DecimalFormat formatter = new DecimalFormat("#,###.00");

        for (Invoice invoice : invoiceArrayList) {
            totalAmount += invoice.getTotalGross();
        }

        String amountLabel = formatter.format(totalAmount);
        tvTotal.setText("₱" + String.valueOf(addZeroes(amountLabel)));
    }

    @Override
    public void onFilterDone(Bundle bundle) {

        Date dateFrom = DateTimeHandler.convertStringtoDate(bundle.getString("DATE_FROM"));
        Date dateTo = DateTimeHandler.convertStringtoDate(bundle.getString("DATE_TO"));

        fetchCustomerInvoices("", DateTimeHandler.convertDatetoDisplayDate(dateTo), DateTimeHandler.convertDatetoDisplayDate(dateFrom));
    }

    // Online functions
    private void fetchCustomerInvoices(String search, String dateFrom, String dateTo) {
        try {
            RequestParams params = new RequestParams();
            params.put("from_mobile", true);
            params.put("search_text", search);
            params.put("date_from", dateFrom);
            params.put("date_to", dateTo);

            HttpClientProvider.get(context, "read_customer_invoices/", params, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        swipeLayout.setRefreshing(false);
                        invoiceArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Invoice>>() {
                        }.getType());
                        readRecords();
                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, "Can't connect to server").show();
                    swipeLayout.setRefreshing(false);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    Toasty.error(context, "Can't connect to server").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toasty.error(context, " " + responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFinish() {
                    swipeLayout.setRefreshing(false);
                }
            });


        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }
    }

    private void showEmptyView(boolean show) {
        viewEmptyListIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        listView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        fetchCustomerInvoices("", null, null);
    }

    private String addZeroes(String val) {
        if (val.equals(".00")) val = "0" + val;
        return val;
    }

}
