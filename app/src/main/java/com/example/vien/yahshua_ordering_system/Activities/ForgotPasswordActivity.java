package com.example.vien.yahshua_ordering_system.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Classes.GlobalVariables;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;
import com.example.vien.yahshua_ordering_system.Utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

import static android.content.Intent.FLAG_ACTIVITY_NO_HISTORY;

public class ForgotPasswordActivity extends AppCompatActivity {

    // Context
    private Context context;

    //Widgets
    private AutoCompleteTextView etEmail, etCode, etPassword, etConfirmPassword;
    private ProgressBar progressBar;
    private LinearLayout layoutFields;
    private Button btnSubmit;
    private TextView tvAuthenticating;
    private ProgressDialog progressDialog;

    //Data
    private String code, email,password,confirmPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context =  this;

        setTitle("Forgot Password");
        initializeUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
       if(etEmail.getText().toString().equals("")){
           super.onBackPressed();
       }else {
           closeSession();
       }
    }

    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        ForgotPasswordActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        GlobalVariables.isRequestSuccess = false;
                        GlobalVariables.email = "";
                        ForgotPasswordActivity.super.onBackPressed();
                        break;
                }
            }
        };

        PopUpProvider.buildYesNoDialog(context, "", "Do you want to continue to change your password? ", dialogClickListener);
    }

    // Initialize component from a layout file
    private void initializeUI() {

        etEmail = findViewById(R.id.et_email);
        etCode = findViewById(R.id.et_code);
        etPassword = findViewById(R.id.et_password);
        etConfirmPassword = findViewById(R.id.et_confirm_password);
        progressBar = findViewById(R.id.spinner);
        layoutFields = findViewById(R.id.layout_fields);
        btnSubmit = findViewById(R.id.btn_submit);
        tvAuthenticating = findViewById(R.id.tv_authenticating);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GlobalVariables.isRequestSuccess){
                    attempToChangePassword();
                } else{
                    attempSubmitEmail();
                }
            }
        });
        showOtherFields(GlobalVariables.isRequestSuccess);
    }

    private void attempToChangePassword()
    {
        try
        {
            // Reset errors.
            etCode.setError(null);
            etEmail.setError(null);
            etPassword.setError(null);
            etConfirmPassword.setError(null);

            // Store values at the time of the submit attempt.
            code = etCode.getText().toString();
            email = etEmail.getText().toString();
            password = etPassword.getText().toString();
            confirmPass = etConfirmPassword.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                etPassword.setError(getString(R.string.error_invalid_password));
                focusView = etPassword;
                cancel = true;
            }

            if (TextUtils.isEmpty(code)){
                etCode.setError(getString(R.string.error_field_required));
                focusView = etCode;
                cancel = true;
            }

            if (TextUtils.isEmpty(password)) {
                etPassword.setError(getString(R.string.error_field_required));
                focusView = etPassword;
                cancel = true;
            }

            if (TextUtils.isEmpty(confirmPass)) {
                etConfirmPassword.setError(getString(R.string.error_field_required));
                focusView = etConfirmPassword;
                cancel = true;
            }
            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                etEmail.setError(getString(R.string.error_field_required));
                focusView = etEmail;
                cancel = true;
            } else if (!isEmailValid(email)) {
                etEmail.setError(getString(R.string.error_invalid_email));
                focusView = etEmail;
                cancel = true;
            }else if (isPasswordValid(password) && !isConfirmPasswordValid(confirmPass)) {
                etConfirmPassword.setError(getString(R.string.error_invalid_password));
                focusView = etConfirmPassword;
                cancel = true;
            }else if (!isPasswordValid(password) && !isConfirmPasswordValid(confirmPass)) {
                etPassword.setError(getString(R.string.error_invalid_password));
                focusView = etPassword;
                cancel = true;
            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                if (!Utility.haveNetworkConnection(context)) {
                    etConfirmPassword.setError("Internet connection is required");
                    etConfirmPassword.requestFocus();
                } else {
                    hideKeyBoard();
                    changePasswordViaEmail();
                }
            }

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    private void attempSubmitEmail(){

        try{

            etEmail.setError(null);
            email = etEmail.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                etEmail.setError(getString(R.string.error_field_required));
                focusView = etEmail;
                cancel = true;
            } else if (!isEmailValid(email)) {
                etEmail.setError(getString(R.string.error_invalid_email));
                focusView = etEmail;
                cancel = true;

            } if (cancel) {
                focusView.requestFocus();
            }else{

                if (!Utility.haveNetworkConnection(context)) {
                    etEmail.setError("Internet connection is required");
                    etEmail.requestFocus();
                } else {
                    hideKeyBoard();
                    requestVerificationEmail();
                }
            }

        } catch(Exception err){
            Toasty.error(context, err.toString()).show();
        }

    }
    private void requestVerificationEmail(){

        try{

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", etEmail.getText().toString());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context,"request_verification_via_email/", stringEntity, new JsonHttpResponseHandler(){
                @Override
                public void onStart() {
                    super.onStart();
                    showSpinner(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    try {

                        Toasty.success(context, "Verification code was successfully sent to your email", Toast.LENGTH_LONG).show();
                        GlobalVariables.isRequestSuccess = true;
                        GlobalVariables.email = etEmail.getText().toString();
                        etEmail.setFocusable(false);

                        showOtherFields(GlobalVariables.isRequestSuccess);
                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    etEmail.setError(errorResponse.toString());
                    etEmail.requestFocus();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    etEmail.setError(errorResponse.toString());
                    etEmail.requestFocus();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    etEmail.setError(responseString);
                    etEmail.requestFocus();
                }

                @Override
                public void onFinish() {
                    showSpinner(false);
                }

            });

        }catch (Exception err){
            Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void changePasswordViaEmail(){

        try{

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("verification_code", Integer.parseInt(etCode.getText().toString()));
            jsonObject.put("email", etEmail.getText().toString());
            jsonObject.put("new_password1", etPassword.getText().toString());
            jsonObject.put("new_password2", etConfirmPassword.getText().toString());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            Debugger.printO(jsonObject);
            HttpClientProvider.post(context,"change-password-via-email-verification/", stringEntity, new JsonHttpResponseHandler(){
                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    try {
                        Toasty.success(context, "Password Successfully change", Toast.LENGTH_LONG).show();
                        GlobalVariables.isRequestSuccess = false;
                        GlobalVariables.email = "";

                        Intent main_intent = new Intent(context, MainActivity.class);
                        main_intent.setFlags(FLAG_ACTIVITY_NO_HISTORY);
                        main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(main_intent);
                        finish();

                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    etConfirmPassword.setError(errorResponse.toString());
                    etConfirmPassword.requestFocus();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    etConfirmPassword.setError(errorResponse.toString());
                    etConfirmPassword.requestFocus();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    if(responseString.contains("Code")){
                        etCode.setError(responseString);
                        etCode.requestFocus();
                    } else {
                        etConfirmPassword.setError(responseString);
                        etConfirmPassword.requestFocus();
                    }
                }

                @Override
                public void onFinish() {
                    progressDialog.dismiss();
                }

            });

        }catch (Exception err){
            Toasty.error(context, err.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 7;
    }

    private boolean isConfirmPasswordValid(String confirmPassword) {
        //TODO: Replace this with your own logic
        return confirmPassword.length() > 7;
    }

    //Hide Keyboard when submit credentials
    private void hideKeyBoard(){
        try
        {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSpinner(boolean show){
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        tvAuthenticating.setVisibility(show ? View.VISIBLE : View.GONE);
        btnSubmit.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showOtherFields(boolean show){
        layoutFields.setVisibility(show ? View.VISIBLE : View.GONE);
        etEmail.setText(GlobalVariables.email);

        if(!etEmail.getText().toString().equals("")){
            etEmail.setFocusable(false);
        }
    }
}
