package com.example.vien.yahshua_ordering_system.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Activities.AccountActivity;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.KeyboardHandler;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class ProfileFragment extends Fragment {

    private Context context;
    private View view;

    private ProgressDialog progressDialog;
    private EditText etFirstName, etLastName, etEmail, etContactNumber;
    private Button btnEditUpdate;

    private boolean isEditable = false;
    private boolean updateSuccess = false;

    //Data
    private ArrayList<UserAccount> userAccountArrayList = new ArrayList<>();

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        KeyboardHandler.hideKeyboard(getActivity());

        context = getContext();
        initializeUI();

    }

    private void initializeUI() {
        etFirstName = view.findViewById(R.id.et_ProfileFirstName);
        etLastName = view.findViewById(R.id.et_ProfileLastName);
        etEmail = view.findViewById(R.id.et_ProfileEmail);
        etContactNumber = view.findViewById(R.id.et_ProfileContactNumber);
        btnEditUpdate = view.findViewById(R.id.btn_ProfileEditUpdate);

        getUserProfile();

        btnEditUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEditable) {
                    enableEditText();
                } else {
                    updateUserProfile();
                }
            }
        });
    }

    private void displayEmployeeDetails() {
        etFirstName.setText(UserSession.getFirstName(getContext()));
        etLastName.setText(UserSession.getLastName(getContext()));
        etContactNumber.setText(UserSession.getContactNumber(getContext()));
        etEmail.setText(UserSession.getEmployeeEmail(getContext()));

//        showProfilePic();
    }

    public void getUserProfile() {
        try {

            HttpClientProvider.get(context, "customer_profile/", null, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>() {
                        }.getType());
                        session.saveUserSession2(context);
                        if (session.saveUserSession2(context)) {
                            displayEmployeeDetails();
                            ((MainActivity) getActivity()).updateNavName(context);
                        }

                        if (updateSuccess) {

//                            Toasty.success(context, "Update success", Toast.LENGTH_SHORT).show();

                            //Update account in database
                            String searchQuery;
                            searchQuery = " WHERE e_mail LIKE '%" + UserSession.getEmployeeEmail(context) + "%' ";
                            userAccountArrayList = UserAccount.read(context, searchQuery);

                            for (UserAccount userAccount : userAccountArrayList) {
                                userAccount.setFirstName(UserSession.getFirstName(getContext()));
                                userAccount.setLastName(UserSession.getLastName(getContext()));
                                userAccount.setEmail(UserSession.getEmployeeEmail(getContext()));
                                userAccount.setToken(UserSession.getToken(getContext()));
                                userAccount.save(context);
                            }

                        }

                    } catch (Exception err) {
//                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Debugger.printO("JSONObject " + throwable.toString());
//                    Toasty.error(context, "Server Error").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    Toasty.error(context, responseString).show();
                }

                @Override
                public void onFinish() {
                    progressDialog.dismiss();
                }
            });


        } catch (Exception err) {
//            Toasty.error(context, err.toString()).show();
        }
    }

    private void updateUserProfile() {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("first_name", etFirstName.getText().toString());
            jsonObject.put("last_name", etLastName.getText().toString());
            jsonObject.put("contact_number", etContactNumber.getText().toString());
//            jsonObject.put("ypo_employee_id", etEmail.getText().toString());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "customer_profile/", stringEntity, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        super.onSuccess(statusCode, headers, response);

                        getUserProfile();
                        updateSuccess = true;
                        disableEditText();
                        Toasty.success(context, "Update success", Toast.LENGTH_SHORT).show();

                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, "Server Error ====" + errorResponse.toString()).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, errorResponse.toString()).show();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toasty.error(context, responseString).show();
                }

                @Override
                public void onFinish() {
                    progressDialog.dismiss();
                }

            });

        } catch (Exception err) {
//            Toasty.error(context, err.toString()).show();
        }
    }

    private void enableEditText() {
        etFirstName.setEnabled(true);
        etLastName.setEnabled(true);
        etContactNumber.setEnabled(true);
        btnEditUpdate.setText("Update");
        isEditable = true;
    }

    private void disableEditText() {
        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etContactNumber.setEnabled(false);
        btnEditUpdate.setText("Edit");
        isEditable = false;
    }

}
