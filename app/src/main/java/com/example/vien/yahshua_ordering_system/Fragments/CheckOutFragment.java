package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Activities.PaymentMethodActivity;
import com.example.vien.yahshua_ordering_system.Adapter.CheckoutAdapter;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.navigation.Navigation;
import es.dmoral.toasty.Toasty;

public class CheckOutFragment extends Fragment {

    // Views and Context
    private Context context;
    private View view;


    // Adapter
    private CheckoutAdapter selectedItemsAdapter;

    // Widgets
    private ListView listView;
    private Button checkout;
    private TextView tvSubTotal, tvShippingFee, tvTotalOrder;
    private View viewEmptyListIndicator;

    // ArrayList and Data
    private ArrayList<Checkout> checkoutArrayList = new ArrayList<>();
    private double subTotal;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_checkout, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();

        getActivity().setTitle("Shopping Cart");

        initializeUI(view);
        readRecords();
        refreshTotal();
    }

    // Override Function to Change Options Menu When this fragment is called
    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_remove_all) {
            confirmRemoveAll();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }

    private void initializeUI(View view) {


        listView = view.findViewById(R.id.lvCheckout);
        checkout = view.findViewById(R.id.btnCheckout);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        tvShippingFee = view.findViewById(R.id.tvShippingfee);
        tvTotalOrder = view.findViewById(R.id.tvTotalOrder);
        viewEmptyListIndicator = view.findViewById(R.id.viewEmptyListIndicator);

        selectedItemsAdapter = new CheckoutAdapter(context, CheckOutFragment.this, R.layout.listrow_selected_item, checkoutArrayList);
        listView.setAdapter(selectedItemsAdapter);

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(subTotal != 0) {

                    //Update the item quantity
                    for(Checkout checkout : checkoutArrayList){
                        checkout.setItemQuantity(checkout.getItemQuantity());
                        checkout.save(context);
                    }

                    CheckOutFragmentDirections.ActionCheckOutFragmentToPaymentMethodFragment action;
                    action = new CheckOutFragmentDirections.ActionCheckOutFragmentToPaymentMethodFragment(String.valueOf(subTotal));
                    action.setArg1(String.valueOf(subTotal));
                    Navigation.findNavController(view).navigate(action);

                } else {
                    Toasty.warning(context, "Nothing to checkout.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // Refresh total when updating quantity
    public void refreshTotal(){

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        subTotal = selectedItemsAdapter.getSubTotal();
        String subTotalLabel = formatter.format(subTotal);

        tvTotalOrder.setText("₱" + String.valueOf(addZeroes(subTotalLabel)));
    }

    // Read data from database
    private void readRecords(){

        checkoutArrayList = Checkout.read(context);
        selectedItemsAdapter.clear();
        selectedItemsAdapter.addAll(checkoutArrayList);
        selectedItemsAdapter.notifyDataSetChanged();

        showEmptyView(checkoutArrayList.size() < 1);
    }

    private void showEmptyView(boolean show)
    {
        viewEmptyListIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        listView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private String addZeroes(String val){
        if (val.equals(".00")) val = "0" + val;
        return val;
    }

    private void removeAllItems(){
        Checkout checkout = new Checkout();
        checkout.deleteAll(context);
        readRecords();
        refreshTotal();
    }

    private void confirmRemoveAll()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                switch (which)
                {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        removeAllItems();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm remove", "Are you sure you want remove all items?");
    }

}
