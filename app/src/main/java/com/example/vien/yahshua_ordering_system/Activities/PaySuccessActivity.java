package com.example.vien.yahshua_ordering_system.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.R;

import static android.content.Intent.FLAG_ACTIVITY_NO_HISTORY;

public class PaySuccessActivity extends AppCompatActivity {

    // Context
    private Context context;

    // Widgets
    private Button done;
    private TextView tvSuccess;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_success);
        context = this;

        initializeUI();
    }

    // Initialize component from a layout file
    private void initializeUI()
    {

        done = findViewById(R.id.btnDone);
        tvSuccess = findViewById(R.id.labelSuccess);

        done.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setClass(context, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        animateButton();
    }

    private void animateButton()
    {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(150);
        anim.setStartOffset(150);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(10);
        tvSuccess.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setClass(context, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
