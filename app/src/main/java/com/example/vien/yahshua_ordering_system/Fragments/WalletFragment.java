package com.example.vien.yahshua_ordering_system.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Activities.AccountActivity;
import com.example.vien.yahshua_ordering_system.Adapter.WalletRecyclerViewAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.DialogFragments.PayDialog;
import com.example.vien.yahshua_ordering_system.Interfaces.RecyclerViewClick;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.Invoice;
import com.example.vien.yahshua_ordering_system.Model.WalletModel;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletFragment extends Fragment {

    private Context context;
    private View view;

    private Button btnPay, btnRequest, btnTopUp;
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;

    private WalletRecyclerViewAdapter walletRecyclerViewAdapter;
    private ArrayList<WalletModel> walletModelArrayList = new ArrayList<>();


    boolean isEmail = false;
    private String amountLabel = "";


    public WalletFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallet, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();

        amountLabel = ((AccountActivity) context).getBalance();
        initializeUI();
        getWalletData();
    }

    private void initializeUI() {
        btnPay = view.findViewById(R.id.btn_WalletPay);
        btnRequest = view.findViewById(R.id.btn_WalletRequest);
        btnTopUp = view.findViewById(R.id.btn_WalletTopUp);
        recyclerView = view.findViewById(R.id.recyclerView_Wallet);

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toasty.warning(context, "Pay clicked.").show();
//                openPayDialog();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                PayDialog payDialog = PayDialog.newInstance(amountLabel);
                payDialog.show(fm, "PayDialogTag");
            }
        });

        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "Request clicked.").show();
            }
        });

        btnTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "Top Up clicked.").show();
            }
        });

    }

    private void getWalletData() {

        try {
            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("invoice", invoice.getUuid());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "get_wallet_transaction_history/", stringEntity, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
                    progressDialog.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        progressDialog.hide();
                        walletModelArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<WalletModel>>() {
                        }.getType());
                        displayWalletData();
                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    Toasty.error(context, "Can't connect to server").show();
                    progressDialog.hide();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    Toasty.error(context, "Can't connect to server").show();
                    progressDialog.hide();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
//                    Toasty.error(context, " " + responseString).show();
                    progressDialog.hide();
                }

                @Override
                public void onFinish() {
                    progressDialog.hide();
                }

            });
        } catch (Exception err) {
//            Toasty.error(context, err.toString()).show();
        }
    }

    private void displayWalletData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        walletRecyclerViewAdapter = new WalletRecyclerViewAdapter(context, walletModelArrayList, new RecyclerViewClick() {
            @Override
            public void onItemClick(View v, int position) {
                openTransactionDialog(walletModelArrayList.get(position));
            }
        });
        recyclerView.setAdapter(walletRecyclerViewAdapter);
    }

    private void openPayDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_wallet_pay, null);
        final TextView tvBalance = dialogView.findViewById(R.id.tv_PayBalance);
        final TextView tvEmailAccNo = dialogView.findViewById(R.id.tv_PayEmailAccNo);
        final EditText etEmailAccNo = dialogView.findViewById(R.id.et_PayEmailAccNo);
        final EditText etAmount = dialogView.findViewById(R.id.et_PayAmount);
        final Button btnDone = dialogView.findViewById(R.id.btn_PayDone);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        tvEmailAccNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmail) {
                    etEmailAccNo.setHint("Email");
                    etEmailAccNo.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    etEmailAccNo.setText("");
                    tvEmailAccNo.setText("Use Account No?");
                    isEmail = false;
                } else {
                    etEmailAccNo.setHint("Account No");
                    etEmailAccNo.setInputType(InputType.TYPE_CLASS_NUMBER);
                    etEmailAccNo.setText("");
                    tvEmailAccNo.setText("Use Email?");
                    isEmail = true;
                }
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Toasty.success(context, "Sent.").show();
            }
        });

        b.show();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void openTransactionDialog(WalletModel walletModel) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_wallet_transaction, null);
        final TextView tvSentReceived = dialogView.findViewById(R.id.tv_WalletSentReceived);
        final TextView tvTransNo = dialogView.findViewById(R.id.tv_WalletTransNo);
        final TextView tvTransAmount = dialogView.findViewById(R.id.tv_WalletTransAmount);
        final TextView tvTransFullName = dialogView.findViewById(R.id.tv_WalletTransFullName);
        final TextView tvTransEmail = dialogView.findViewById(R.id.tv_WalletTransEmail);
        final TextView tvTransDateTime = dialogView.findViewById(R.id.tv_WalletTransDateTime);
        final TextView tvTransRemark = dialogView.findViewById(R.id.tv_WalletTransRemark);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String amountLabel = formatter.format(walletModel.getAmount());

        tvSentReceived.setText(walletModel.getTransactionType());
        tvTransNo.setText(walletModel.getTransactionNumber());
        tvTransAmount.setText(String.valueOf("₱" + amountLabel));
        tvTransDateTime.setText(DateTimeHandler.convertDisplayDate(walletModel.getDate()) + " | " + DateTimeHandler.formatTime(walletModel.getDate()));
        tvTransRemark.setText(walletModel.getRemarks());
        if (walletModel.getTransactionType().equals("Received")) {
            tvTransFullName.setText(walletModel.userSource.getFullName());
            tvTransEmail.setText(walletModel.userSource.getEmail());
        } else {
            tvTransFullName.setText(walletModel.userDestination.getFullName());
            tvTransEmail.setText(walletModel.userDestination.getEmail());
        }

        b.show();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


}
