package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class MapFields implements Parcelable {

    private String name;

    public MapFields(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected MapFields(Parcel in) {
        name = in.readString();
    }

    public static final Creator<MapFields> CREATOR = new Creator<MapFields>() {
        @Override
        public MapFields createFromParcel(Parcel in) {
            return new MapFields(in);
        }

        @Override
        public MapFields[] newArray(int size) {
            return new MapFields[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
    }
}
