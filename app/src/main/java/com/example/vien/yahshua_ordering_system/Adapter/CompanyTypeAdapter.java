package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Model.CompanyType;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.util.ArrayList;

public class CompanyTypeAdapter extends RecyclerView.Adapter<CompanyTypeAdapter.ViewHolder> {

    private ArrayList<CompanyType> mData;
    private ArrayList<String> image;
    private LayoutInflater mInflater;
    private ItemCompanyTypeClickListener mClickListener;
    private Context context;
    private Fragment fragment;


    // data is passed into the constructor
    public CompanyTypeAdapter(Context context, Fragment fragment, ArrayList<CompanyType> data, ArrayList<String> image) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.fragment = fragment;
        this.image = image;
        this.context = context;
    }

    @Override
    public CompanyTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.company_header_images, parent, false);
        return new CompanyTypeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CompanyType companyType = mData.get(position);
        holder.tvName.setText(companyType.getName());

//        RequestOptions myOption = new RequestOptions()
//                .circleCrop()
//                .centerInside();
//        Glide.with(fragment)
//                .load(getImage(image.get(position)))
//                .apply(myOption)
//                .into(holder.imageView);

        if (companyType.getName().equals("All")) {
            holder.imageView.setImageResource(R.drawable.all);
        } else if (companyType.getName().equals("Shopping")) {
            holder.imageView.setImageResource(R.drawable.shopping);
        } else if (companyType.getName().equals("Restaurant")) {
            holder.imageView.setImageResource(R.drawable.restaurant);
        } else if (companyType.getName().equals("Hotel")) {
            holder.imageView.setImageResource(R.drawable.hotel);
        } else if (companyType.getName().equals("Services")) {
            holder.imageView.setImageResource(R.drawable.services);
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<CompanyType> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        CardView cardView;
        TextView tvName;
        RatingBar ratingBar;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
//            cardView = itemView.findViewById(R.id.cv_category_list);
            imageView = itemView.findViewById(R.id.iv_name);
            tvName = itemView.findViewById(R.id.tv_name);
            ratingBar = itemView.findViewById(R.id.ratingBar_Header);

            imageView.setOnClickListener(this);
//            cardView.setOnClickListener(this);
//            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemCompanyTypeClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    // convenience method for getting data at click position
    public CompanyType getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemCompanyTypeClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemCompanyTypeClickListener {
        void onItemCompanyTypeClick(View view, int position);
    }

    public int getImage(String imageName) {

        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());

        return drawableResourceId;
    }


}
