package com.example.vien.yahshua_ordering_system.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.vien.yahshua_ordering_system.DBClasses.DatabaseAdapter;
import com.example.vien.yahshua_ordering_system.Utils.Converter;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserAccount implements Parcelable{

    private int id;
    @SerializedName("token")
    private String token;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    private String email;
    @SerializedName("ypo_employee_id")
    private String  ypoEmployeeId;

    public UserAccount() {

    }

    public String getYpoEmployeeId() {
        return ypoEmployeeId;
    }

    public void setYpoEmployeeId(String ypoEmployeeId) {
        this.ypoEmployeeId = ypoEmployeeId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected UserAccount(Parcel in) {
        token = in.readString();
        firstName = in.readString();
        lastName = in.readString();
    }

    public static int checkUserCount(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        int count = 0;
        String query = "SELECT COUNT(id) as count FROM user ORDER BY id DESC ";

        Cursor cursor = db.open().getDatabaseInstance().rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {
                count = (Integer.parseInt(cursor.getString(cursor.getColumnIndex("count"))));
            } while (cursor.moveToNext());
        }

        return count;
    }

    public static ArrayList<UserAccount> read(Context context,  String searchQuery) {
        ArrayList<UserAccount> userAccountArrayList = new ArrayList<UserAccount>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery = "SELECT * FROM user ";

            if (searchQuery != null) selectQuery += " " + searchQuery + " ORDER BY id DESC";

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    UserAccount userAccount = new UserAccount();

                    userAccount.setId(Converter.ObjectInt(cursor, "id"));
                    userAccount.setFirstName(cursor.getString(cursor.getColumnIndex("first_name")));
                    userAccount.setLastName(cursor.getString(cursor.getColumnIndex("last_name")));
                    userAccount.setToken(cursor.getString(cursor.getColumnIndex( "token")));
                    userAccount.setEmail(cursor.getString(cursor.getColumnIndex( "e_mail")));

                    userAccountArrayList.add(userAccount);

                } while (cursor.moveToNext());

            }

            cursor.close();
            DatabaseAdapter.close();

            return userAccountArrayList;

        } catch (Exception err) {

            return userAccountArrayList;
        }
    }

    public boolean save(Context context){
        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("first_name", getFirstName());
            contentValues.put("last_name", getLastName());
            contentValues.put("token",getToken());
            contentValues.put("e_mail", getEmail());

            if (getId() != 0) {
                db.open();
                db.update(contentValues, "user", " id = ?", new String[]{ String.valueOf(getId()) });
            } else {
                db.save(contentValues, "user");
            }

            return true;
        } catch (Exception err) {
            return false;
        }
    }

    public void delete(Context context) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        if (getId() == 0) {
            db.delete("user", "id = ?", new String[]{String.valueOf(getId())});
            return;
        }

        db.delete("user", "id = ?", new String[]{String.valueOf(getId())});
    }

    public static final Creator<UserAccount> CREATOR = new Creator<UserAccount>() {
        @Override
        public UserAccount createFromParcel(Parcel in) {
            return new UserAccount(in);
        }

        @Override
        public UserAccount[] newArray(int size) {
            return new UserAccount[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(firstName);
        dest.writeString(lastName);
    }
}
