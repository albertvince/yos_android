package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.google.gson.annotations.SerializedName;

public class Company implements Parcelable {

    @SerializedName("company_name")
    private String companyName;
    @SerializedName("uuid")
    private String uuId;
    @SerializedName("avatar")
    private String companyLogo;
    @SerializedName("companytype")
    private CompanyType companyType;


    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    public String getImageURL(){
        return HttpClientProvider.getBaseURL() + getCompanyLogo();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    protected Company(Parcel in) {
        companyName = in.readString();
        uuId = in.readString();
        companyLogo = in.readString();
        companyType = in.readParcelable(CompanyType.class.getClassLoader());
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(companyName);
        dest.writeString(uuId);
        dest.writeString(companyLogo);
        dest.writeParcelable(companyType, flags);
    }
}
