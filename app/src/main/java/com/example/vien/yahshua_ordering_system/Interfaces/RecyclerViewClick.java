package com.example.vien.yahshua_ordering_system.Interfaces;

import android.view.View;

public interface RecyclerViewClick {
    void onItemClick(View v, int position);
}
