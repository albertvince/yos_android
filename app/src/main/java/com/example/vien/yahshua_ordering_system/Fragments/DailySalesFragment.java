package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

import com.example.vien.yahshua_ordering_system.Adapter.DailySalesRecyclerViewAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.DailySales;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class DailySalesFragment extends Fragment  implements  DailySalesRecyclerViewAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener{

    // Views and Context
    private View view;
    private Context context;

    //Widgets
    private SwipeRefreshLayout swipeLayout;
    private View viewEmptyListIndicator;
    private RecyclerView rcDailySales;
    DailySalesRecyclerViewAdapter dailySalesRecyclerViewAdapter;

    //Data
    private ArrayList<DailySales> dailySalesArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_daily_sales, container, false);

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = view.getContext();

        initializeUI(view);
        fetchDailySales("");
    }

    private void initializeUI(View view){

        viewEmptyListIndicator = view.findViewById(R.id.viewEmptyListIndicator);
        rcDailySales = view.findViewById(R.id.rc_daily_sales);

        ((MainActivity) context).tabLayout.setVisibility(View.GONE);

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));
    }

    // Read data from database
    private void readRecords() {
        rcDailySales.setLayoutManager(new LinearLayoutManager(context));
        dailySalesRecyclerViewAdapter = new DailySalesRecyclerViewAdapter(context, DailySalesFragment.this, dailySalesArrayList);
        dailySalesRecyclerViewAdapter.setClickListener(this);
        rcDailySales.setAdapter(dailySalesRecyclerViewAdapter);
        registerForContextMenu(rcDailySales);

        showEmptyView(dailySalesArrayList.size() < 1);
    }

    private void showEmptyView(boolean show)
    {
        viewEmptyListIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        rcDailySales.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    // Online functions
    private void fetchDailySales(String search) {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("sales", search);

            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            Debugger.printO(stringEntity);
            HttpClientProvider.post(context, "read_daily_sales/", stringEntity, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        super.onSuccess(statusCode, headers, response);

                        swipeLayout.setRefreshing(false);
                        dailySalesArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<DailySales>>() {}.getType());
                        readRecords();
                    } catch (Exception err) {
                        Toasty.error(context,"onSuccess "+ err.toString()).show();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    swipeLayout.setRefreshing(false);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.error(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                    showEmptyView(dailySalesArrayList.size() < 1);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }

                @Override
                public void onFinish() {
                    swipeLayout.setRefreshing(false);
                }
            });

        } catch (Exception err) {
            Toasty.error(context, "Exception " + err.toString()).show();
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        dailySalesRecyclerViewAdapter.getItem(position);
    }

    public void gotoProduct(int position){

        Bundle bundle = new Bundle();
        bundle.putString("SALES_PROMO", dailySalesRecyclerViewAdapter.getItem(position).getUuid());
        ((MainActivity) getActivity()).replaceFragment(ProductsFragment.class, bundle);
    }

    @Override
    public void onRefresh() {
        fetchDailySales("");
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }
}
