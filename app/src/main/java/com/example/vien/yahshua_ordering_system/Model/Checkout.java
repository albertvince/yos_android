package com.example.vien.yahshua_ordering_system.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Debug;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.DBClasses.DatabaseAdapter;
import com.example.vien.yahshua_ordering_system.Utils.Converter;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class Checkout implements Parcelable {

    private int id;
    private String itemName;
    @SerializedName("uuid")
    private String uuId;
    private String itemImage;
    @SerializedName("price")
    private double itemPrice;
    @SerializedName("quantity")
    private int itemQuantity;
    private int itemTotal;
    private Date date;
    private boolean isCheckout;
    @SerializedName("inventory")
    private int inventory;
    @SerializedName("invoice_products")
    private ArrayList<InvoiceProducts> invoiceProducts;
    @SerializedName("category")
    private String category;
    @SerializedName("set_person_count")
    private int personCount;
    @SerializedName("set_arrival_time")
    private String arrivalTime;
    @SerializedName("set_arrival_date")
    private String arrivalDate;

    @SerializedName("invoice_payment")
    private InvoicePayment invoicePayment;

    public InvoicePayment getInvoicePayment() {
        return invoicePayment;
    }

    public void setInvoicePayment(InvoicePayment invoicePayment) {
        this.invoicePayment = invoicePayment;
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<InvoiceProducts> getInvoiceProducts() {
        return invoiceProducts;
    }

        public void setInvoiceProducts(ArrayList<InvoiceProducts> invoiceProducts) {
        this.invoiceProducts = invoiceProducts;
    }

    public int getInventory() {
        return inventory;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isCheckout() {
        return isCheckout;
    }

    public void setCheckout(boolean checkout) {
        isCheckout = checkout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getImageURL(){
        return HttpClientProvider.getBaseURL() + getItemImage();
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public int getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(int itemTotal) {
        this.itemTotal = itemTotal;
    }

    public void addQuantity(double num){
        this.itemQuantity += num;
    }
    public Checkout(){

    }

    public static int getTotalOrder(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        int quantity;
        int price;
        int total = 0;
        String query = "SELECT id, quantity, price FROM cart WHERE is_checkout = 0 ORDER BY id DESC ";

        Cursor cursor = db.open().getDatabaseInstance().rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {
                quantity = (Integer.parseInt(cursor.getString(cursor.getColumnIndex("quantity"))));
                price = (Integer.parseInt(cursor.getString(cursor.getColumnIndex("price"))));
                total += quantity *  price;
            } while (cursor.moveToNext());
        }

        return total;
    }

    public static int getTotalItemCount(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        int count = 0;
        String query = "SELECT COUNT(id) as count FROM cart WHERE is_checkout = 0 AND quantity > 0 ORDER BY id DESC ";

        Cursor cursor = db.open().getDatabaseInstance().rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {
                count = (Integer.parseInt(cursor.getString(cursor.getColumnIndex("count"))));
            } while (cursor.moveToNext());
        }

        return count;
    }

    public static ArrayList<Checkout> read(Context context)
    {
        ArrayList<Checkout> checkoutArrayList = new ArrayList<Checkout>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery = "SELECT * FROM cart WHERE  is_checkout = 0 AND quantity > 0 ORDER BY id DESC";

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Checkout checkout = new Checkout();

                    checkout.setId(Converter.ObjectInt(cursor, "id"));
                    checkout.setItemName(cursor.getString(cursor.getColumnIndex("name")));
                    checkout.setItemImage(cursor.getString(cursor.getColumnIndex("image")));
                    checkout.setItemQuantity(Converter.ObjectInt(cursor,"quantity"));
                    checkout.setItemPrice(Integer.parseInt(cursor.getString(cursor.getColumnIndex("price"))));
                    checkout.setItemTotal(Integer.parseInt(cursor.getString(cursor.getColumnIndex("total"))));
                    checkout.setDate(DateTimeHandler.convertStringtoDate(cursor.getString(cursor.getColumnIndex("date"))));
                    checkout.setCheckout(Converter.ObjectToBoolean(cursor, "is_checkout"));
                    checkout.setUuId(cursor.getString(cursor.getColumnIndex("uuId")));
                    checkout.setCategory(Converter.ObjectToString(cursor, "category"));

                   checkoutArrayList.add(checkout);

                } while (cursor.moveToNext());

            }

            cursor.close();
            DatabaseAdapter.close();
            return checkoutArrayList;

        } catch (Exception err) {

            return checkoutArrayList;
        }
    }

    public static ArrayList<Checkout> readForCheckout(Context context, String uuid)
    {
        ArrayList<Checkout> checkoutArrayList = new ArrayList<Checkout>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery = "SELECT * FROM cart WHERE  is_checkout = 0 AND quantity > 0 AND uuID LIKE '%"+uuid+"%'";
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Checkout checkout = new Checkout();

                    checkout.setId(Converter.ObjectInt(cursor, "id"));
                    checkout.setItemName(cursor.getString(cursor.getColumnIndex("name")));
                    checkout.setItemImage(cursor.getString(cursor.getColumnIndex("image")));
                    checkout.setItemQuantity(Converter.ObjectInt(cursor,"quantity"));
                    checkout.setItemPrice(Integer.parseInt(cursor.getString(cursor.getColumnIndex("price"))));
                    checkout.setItemTotal(Integer.parseInt(cursor.getString(cursor.getColumnIndex("total"))));
                    checkout.setDate(DateTimeHandler.convertStringtoDate(cursor.getString(cursor.getColumnIndex("date"))));
                    checkout.setCheckout(Converter.ObjectToBoolean(cursor, "is_checkout"));
                    checkout.setUuId(cursor.getString(cursor.getColumnIndex("uuId")));
                    checkout.setCategory(Converter.ObjectToString(cursor, "category"));

                    checkoutArrayList.add(checkout);

                } while (cursor.moveToNext());

            }
            return checkoutArrayList;

        } catch (Exception err) {

            return checkoutArrayList;
        }
    }

    public boolean save(Context context){
        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("name", getItemName());
            contentValues.put("image", getItemImage());
            contentValues.put("price",getItemPrice());
            contentValues.put("quantity",getItemQuantity());
            contentValues.put("total",getItemTotal());
            contentValues.put("date", DateTimeHandler.convertDatetoStringDate(getDate()));
            contentValues.put("is_checkout", Converter.BooleanToInt(isCheckout()));
            contentValues.put("uuId", getUuId());
            contentValues.put("category", getCategory());

            if (getId() != 0) {
                db.open();
                db.update(contentValues, "cart", " id = ?", new String[]{ String.valueOf(getId()) });
            } else {
                db.save(contentValues, "cart");
            }

            return true;
        } catch (Exception err) {

            return false;
        }
    }

    public void delete(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);

        if (getId() == 0)
        {
            db.delete("cart","id = ?", new String[]{ String.valueOf(getId()) });
            return;
        }

        db.delete("cart","id = ?", new String[]{  String.valueOf(getId())  });
    }

    public void deleteAll(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);

        if (getId() == 0)
        {
            db.delete("cart",null, null);
            return;
        }

        db.delete("cart",null, null);
    }

    protected Checkout(Parcel in) {
        itemName = in.readString();
        itemPrice = in.readInt();
        itemQuantity = in.readInt();
        inventory = in.readInt();

    }

    public static final Creator<Checkout> CREATOR = new Creator<Checkout>() {
        @Override
        public Checkout createFromParcel(Parcel in) {
            return new Checkout(in);
        }

        @Override
        public Checkout[] newArray(int size) {
            return new Checkout[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemName);
        dest.writeDouble(itemPrice);
        dest.writeInt(itemQuantity);
        dest.writeInt(inventory);
    }
}
