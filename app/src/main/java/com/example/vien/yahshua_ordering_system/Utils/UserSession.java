package com.example.vien.yahshua_ordering_system.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.google.gson.annotations.SerializedName;

import es.dmoral.toasty.Toasty;

public class UserSession {


    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("token")
    private String authToken;
    @SerializedName("emp_id_digits")
    private int empIdDigits;
    @SerializedName("avatar")
    private String profilePicture;
    private String email;
    private String code;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("ypo_employee_id")
    private String ypoEmployeeId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;

    private String localImage;
    public user user = new user();

    public class user{

        @SerializedName("first_name")
        private String firstName;
        @SerializedName("contact_number")
        private String contactNumber;
        @SerializedName("last_name")
        private String lastName;
        private String email;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }
    }

    public String getLocalImage() {
        return localImage;
    }

    public void setLocalImage(String localImage) {
        this.localImage = localImage;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            code = code;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            fullName = fullName;
        }

        public String getYpoEmployeeId() {
            return ypoEmployeeId;
        }

        public void setYpoEmployeeId(String ypoEmployeeId) {
            ypoEmployeeId = ypoEmployeeId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
           firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName){lastName = lastName;
        }

    public String setProfilePic(){
       return  (HttpClientProvider.getBaseURL() + profilePicture).replace("//media","/media");
    }

    public static String getToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("AUTHTOKEN", "");
    }

    public static String getFirstName(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("FIRST_NAME", "");
    }

    public static String getLastName(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("LAST_NAME", "");
    }

    public static String getContactNumber(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CONTACT_NUMBER", "");
    }

    public static String getCompany(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COMPANY_NAME", "");
    }

    public static String getEmployeeEmail(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("EMAIL", "");
    }

    public static String getProfilePicture(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PROFILE_PICTURE", "");
    }

    public static String getYpoEmployeeId(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("YPO_EMPLOYEE_ID", "");
    }

    public static String getEmployeeCode(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CODE", "");
    }

    public static String getLocalImage(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("LOCAL_IMAGE", "");
    }

    public static boolean clearSession(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }

    public boolean saveUserSession(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("AUTHTOKEN", getAuthToken());
        editor.putString("EMAIL",user.email);
        editor.putString("CONTACT_NUMBER",user.contactNumber);
        editor.putString("FIRST_NAME",user.firstName);
        editor.putString("LAST_NAME",user.lastName);
        return editor.commit();
    }

    public boolean saveUserSession2(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("FIRST_NAME", firstName);
        editor.putString("LAST_NAME", lastName);
        editor.putString("CODE", code);
        editor.putString("CONTACT_NUMBER", contactNumber);
        editor.putString("EMAIL", email);
        editor.putString("PROFILE_PICTURE", setProfilePic());
        editor.putString("YPO_EMPLOYEE_ID", ypoEmployeeId);

        return editor.commit();
    }

    public boolean saveLocalImage(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("LOCAL_IMAGE", localImage);

        return editor.commit();
    }

    public boolean removeLocalImage(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("LOCAL_IMAGE", "");

        return editor.commit();
    }


}
