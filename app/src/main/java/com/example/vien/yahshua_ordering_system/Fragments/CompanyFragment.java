package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.vien.yahshua_ordering_system.Activities.StoreAndItemActivity;
import com.example.vien.yahshua_ordering_system.Adapter.StoreAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Model.Store;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class CompanyFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, StoreAdapter.ItemClickListener {

    // Views and Context
    private View view;
    private Context context;

    // Adapter
    private StoreAdapter storeAdapter;

    // Widget
    private RecyclerView rcCompanyList;
    private SwipeRefreshLayout swipeLayout;
    private View viewEmptyListIndicator;
    public TabLayout tabLayout;

    // ArrayList
    private ArrayList<Store> storeArrayList = new ArrayList<>();

    //Data
    private static final String COMPANYNAME = "arg1";
    private static final String COMPANYUUID = "arg2";
    private String companyUuid;
    private String companyName;
    private FragmentManager manager;


    public CompanyFragment(){

    }

    public static CompanyFragment newInstance(String param1, String param2) {
        CompanyFragment fragment = new CompanyFragment();
        Bundle args = new Bundle();
        args.putString(COMPANYNAME, param1);
        args.putString(COMPANYUUID, param2);
        fragment.setArguments(args);
        return fragment;
    }

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_company, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();

        manager = getChildFragmentManager();

        if (getArguments() != null) {
            companyName = getArguments().getString(COMPANYNAME);
            companyUuid = getArguments().getString(COMPANYUUID);
        }

        getActivity().setTitle(companyName);

        initializeUI(view);
        fetchStores();

    }

    // Override Function to Change Options Menu When this fragment is called
    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.category_search, menu);

        final MenuItem searchViewItem = menu.findItem(R.id.action_search);


        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchStores();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchStores();
                return true;
            }

        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    // Initialize component from a layout file
    private void initializeUI(View view){

        rcCompanyList = view.findViewById(R.id.rc_company_list);
        viewEmptyListIndicator = view.findViewById(R.id.viewEmptyListIndicator);

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

    }

    // Read data from database
    private void readRecords()
    {
        rcCompanyList.setLayoutManager(new GridLayoutManager(context, 3));
        storeAdapter = new StoreAdapter(context, CompanyFragment.this, storeArrayList);
        storeAdapter.setClickListener(this);
        rcCompanyList.setAdapter(storeAdapter);
        registerForContextMenu(rcCompanyList);

       showEmptyView(storeArrayList.size() < 1);
    }


    // Online functions
    private void fetchStores()
    {
        try
        {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("company_type", companyUuid);

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "read_store_list/", stringEntity, new JsonHttpResponseHandler()
            {

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        swipeLayout.setRefreshing(false);
                        storeArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Store>>(){}.getType());
                        readRecords();
                    } catch (Exception err)
                    {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.warning(context, "Can't connect to server").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    Toasty.warning(context, "Can't connect to server").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toasty.warning(context, " " + responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    swipeLayout.setRefreshing(false);
                }
            });


        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }

    }

    private void showEmptyView(boolean show)
    {
        viewEmptyListIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        rcCompanyList.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        fetchStores();
    }

    @Override
    public void onItemClick(View view, int position) {

//        openStoreProductFragment();
        ((StoreAndItemActivity) context).openStoreProductFragment();
    }
}
