package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Model.Store;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.util.ArrayList;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {


    private ArrayList<Store> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private Fragment fragment;
    private ImageView imageView;


    // data is passed into the constructor
    public StoreAdapter(Context context, Fragment fragment, ArrayList<Store> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.fragment = fragment;
    }

    @Override
    public StoreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listrow_store, parent, false);
        return new StoreAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Store store = mData.get(position);
        holder.companyName.setText(store.getCompanyName());

        ifImageAvailable(store);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<Store> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        CardView cardView;
        TextView companyName;

        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv_store);
            imageView = itemView.findViewById(R.id.iv_store);
            companyName = itemView.findViewById(R.id.tvCompany);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    // convenience method for getting data at click position
    public Store getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {

        void onItemClick(View view, int position);
    }


    //Display list of image using Glide
    private void displayImage(Store store) {
        RequestOptions myOption = new RequestOptions().centerInside();

        Glide.with(fragment)
                .load(store.getImageURL()
                 .replace("//media", "/media"))
                .apply(myOption).into(imageView);
    }

    private void ifImageAvailable(Store store) {

        if (store.getImageURL().equals(HttpClientProvider.getBaseURL() + "media/default_inventory.jpg") || !store.getImageURL().contains("/media")) {
            Debugger.logD("Default image");
        } else {
            displayImage(store);
        }
    }

}
