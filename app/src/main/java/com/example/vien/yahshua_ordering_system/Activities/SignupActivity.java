package com.example.vien.yahshua_ordering_system.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Toasters;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.example.vien.yahshua_ordering_system.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class SignupActivity extends AppCompatActivity
{

    // Context
    private Context context;
    // Widgets
    private AutoCompleteTextView etFname, etLname, etEmail, etPass, etConfirm;
    private Button btnRegsiter;
    // Data
    private String firstname, lastname, email,password,confirmPass;
    private UserAccount userAccount;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = this;

        initializeUI();

    }

    // Initialize component from a layout file
    private void initializeUI()
    {
        etFname = findViewById(R.id.et_first_name);
        etLname = findViewById(R.id.et_last_name);
        etEmail = findViewById(R.id.etEmail);
        etPass = findViewById(R.id.etPass);
        etConfirm = findViewById(R.id.etConfirmPass);
        btnRegsiter = findViewById(R.id.btnSubmit);

        etConfirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attempRegister();
                    return true;
                }
                return false;
            }
        });

        btnRegsiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempRegister();
            }
        });
    }

    private void attempRegister()
    {
        try
        {
            // Reset errors.
            etFname.setError(null);
            etLname.setError(null);
            etEmail.setError(null);
            etPass.setError(null);
            etConfirm.setError(null);

            // Store values at the time of the login attempt.
            firstname = etFname.getText().toString();
            lastname = etLname.getText().toString();
            email = etEmail.getText().toString();
            password = etPass.getText().toString();
            confirmPass = etConfirm.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                etPass.setError(getString(R.string.error_invalid_password));
                focusView = etPass;
                cancel = true;
            }

            if (TextUtils.isEmpty(firstname)){
                etFname.setError(getString(R.string.error_field_required));
                focusView = etFname;
                cancel = true;
            }

            if (TextUtils.isEmpty(lastname)){
                etLname.setError(getString(R.string.error_field_required));
                focusView = etLname;
                cancel = true;
            }

            if (TextUtils.isEmpty(password)) {
                etPass.setError(getString(R.string.error_field_required));
                focusView = etPass;
                cancel = true;
            }

            if (TextUtils.isEmpty(confirmPass)) {
                etConfirm.setError(getString(R.string.error_field_required));
                focusView = etConfirm;
                cancel = true;
            }
            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                etEmail.setError(getString(R.string.error_field_required));
                focusView = etEmail;
                cancel = true;
            } else if (!isEmailValid(email)) {
                etEmail.setError(getString(R.string.error_invalid_email));
                focusView = etEmail;
                cancel = true;
            }else if (isPasswordValid(password) && !isConfirmPasswordValid(confirmPass)) {
                etConfirm.setError(getString(R.string.error_invalid_password));
                focusView = etConfirm;
                cancel = true;
            }else if (!isPasswordValid(password) && !isConfirmPasswordValid(confirmPass)) {
                etPass.setError(getString(R.string.error_invalid_password));
                focusView = etPass;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.

                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                JSONObject jsonParams = new JSONObject();

                jsonParams.put("email", email);
                jsonParams.put("password", password);
                jsonParams.put("confirm_password",confirmPass);
                jsonParams.put("first_name", firstname);
                jsonParams.put("last_name", lastname);

                StringEntity entity = new StringEntity(jsonParams.toString());

                if (!Utility.haveNetworkConnection(context)) {
                    etConfirm.setError("Internet connection is required");
                    etConfirm.requestFocus();
                } else {
                    hideKeyBoard();
                    register(entity);
                }
            }

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    //Keyboard when submit credentials
    private void hideKeyBoard(){
        try
        {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 7;
    }

    private boolean isConfirmPasswordValid(String confirmPassword) {
        //TODO: Replace this with your own logic
        return confirmPassword.length() > 7;
    }

    private void register(StringEntity entity) throws JSONException  {

        HttpClientProvider.postRegister(context, "register/", entity, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                Toasters.ShowLoadingSpinner(context, false);
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, org.json.JSONObject response) {

                Toasty.success(getApplicationContext(), "You are now registered!", Toast.LENGTH_SHORT).show();
                UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>(){}.getType());
                session.saveUserSession(getApplicationContext());
                if (session.saveUserSession(context)) {
                    finish();
                    Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(main_intent);
                }

                UserAccount userAccount = new UserAccount();

                userAccount.setLastName(UserSession.getLastName(context));
                userAccount.setFirstName(UserSession.getFirstName(context));
                userAccount.setToken(UserSession.getToken(context));
                userAccount.setEmail(UserSession.getEmployeeEmail(context));
                userAccount.save(context);
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                etEmail.setError(responseString);
                etEmail.requestFocus();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                etPass.setError(errorResponse.toString());
                etPass.requestFocus();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                etPass.setError( errorResponse.toString());
                etPass.requestFocus();
            }

            @Override
            public void onFinish() {
                Toasters.HideLoadingSpinner();
            }

            @Override
            public void onRetry(int retryNo) {

            }
        });
    }
}
