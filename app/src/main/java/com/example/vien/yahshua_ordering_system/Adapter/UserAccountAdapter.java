package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Activities.UserAccountActivity;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.InvoiceProduct;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.PopUpProvider;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class UserAccountAdapter extends ArrayAdapter<UserAccount> {

    private ArrayList<UserAccount> userAccountArrayList;
    private ImageView imageView;
    private TextView firstName, email;
    private ImageView imgOption;
    private LinearLayout layoutOption;
    public UserAccountAdapter(@NonNull Context context, int resource, @NonNull ArrayList<UserAccount> arrayList) {
        super(context, resource, arrayList);
        userAccountArrayList = arrayList;
    }

    @Override
    public int getPosition(@Nullable UserAccount item)
    {
        int x = 0;
        for(UserAccount userAccount : userAccountArrayList)
        {
            if(userAccount.getFirstName() == userAccount.getFirstName())
            {
                return x;
            }else{
                x++;
            }
        }

        return super.getPosition(item);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final UserAccount userAccount = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_account, parent, false);
        }

        firstName = convertView.findViewById(R.id.tv_firstname);
        email = convertView.findViewById(R.id.tv_email);
        imageView = convertView.findViewById(R.id.iv_user_image);
        imgOption = convertView.findViewById(R.id.img_more_option);
        layoutOption = convertView.findViewById(R.id.layout_option);

        layoutOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmCancel(userAccount, v);
            }
        });

        if (userAccount.getFirstName() != null && userAccount.getLastName() != null)
        {
            firstName.setText(userAccount.getFirstName() +" "+ userAccount.getLastName());
        }

//        firstName.setVisibility(View.INVISIBLE);
        email.setText(userAccount.getEmail());
//        ifImageAvailable(userAccount);

        return convertView;

    }

    public void confirmCancel(final UserAccount userAccount,View view)
    {

        PopupMenu popup = new PopupMenu(getContext(),view);
        popup.getMenuInflater().inflate(R.menu.popup_menu,popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if(id == R.id.remove){
                    deleteAccount(userAccount);
                    Toasty.success(getContext(), "Removed account", Toast.LENGTH_SHORT).show();
                }if(id == R.id.login){
                    ((UserAccountActivity) getContext()).openLoginDialog(userAccount.getEmail(), userAccount.getFirstName(), userAccount.getLastName(), userAccount.getToken());
                }
                return true;
            }
        });

//        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i)
//            {
//                deleteAccount(userAccount);
//                Toasty.success(getContext(), "Removed account", Toast.LENGTH_SHORT).show();
//            }
//        };
//
//        PopUpProvider.buildYesNoDialog(getContext(),"Remove account",
//                userAccount.getEmail()+" ?", dialogClickListener);
    }

    private void deleteAccount(UserAccount userAccount)
    {
        userAccount.delete(getContext());
        remove(userAccount);
        notifyDataSetChanged();
        ((UserAccountActivity)getContext()).readRecords();
    }

    /* Display list of image using Glide */
//    private void displayImage(UserAccount userAccount)
//    {
//        RequestOptions myOption = new RequestOptions()
//                .centerInside();
//
//        Glide.with(getContext())
//                .load(userAccount.getInventory().getImageURL().replace("//media","/media"))
//                .apply(myOption)
//                .into(imageView);
//
//    }
//
//    private void ifImageAvailable(UserAccount userAccount){
//
//        if (invoiceProduct.getInventory().getImageURL().equals(HttpClientProvider.getBaseURL() + "media/default_inventory.jpg") || !invoiceProduct.getInventory().getImageURL().contains("/media")) {
//            Debugger.logD("Default image");
//        } else {
//            displayImage(invoiceProduct);
//        }
//    }
}
