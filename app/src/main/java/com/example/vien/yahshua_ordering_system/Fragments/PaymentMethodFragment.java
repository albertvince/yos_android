package com.example.vien.yahshua_ordering_system.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Syncer;

import java.text.DecimalFormat;
import java.util.Calendar;

import es.dmoral.toasty.Toasty;

public class PaymentMethodFragment extends Fragment {

    // Views and Context
    private Context context;
    private View view;

    // Widgets
    private Button pay, delivery, salary, paypal;
    private RadioButton cashOnDeliver, salaryDeduction, paypals;
    private TextView totalAmount, paymentType, tvDateNow, tvTimeNow;
    private EditText etCountPerson;

    private TabLayout mTabLayout;
    private CardView cod, sd, pp;
    private TextView total, type;
    private Button btnPay;
    private ConstraintLayout constraintPaymentMethond;
    private ConstraintLayout constraintReservation;

    // ArrayList and Data
    private String paymentMethod = "";
    private String amount;
    private int getHour, getMinute, year, month, day, hour, minute, countPerson = 0;
    private String getTime, getDate;
    private static final String AMOUNT = "arg1";

    Calendar newDate = Calendar.getInstance();

    public PaymentMethodFragment(){

    }

    public static PaymentMethodFragment newInstance(String param1) {
        PaymentMethodFragment fragment = new PaymentMethodFragment();
        Bundle args = new Bundle();
        args.putString(AMOUNT, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_payment_method, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();

        if (getArguments() != null) {
            amount = getArguments().getString(AMOUNT);
        }
        getActivity().setTitle("Select Payment");
        initializeUI(view);
    }

    private void initializeUI(View view){

        pay = view.findViewById(R.id.btnPay);
        cashOnDeliver = view.findViewById(R.id.rdnCash);
        salaryDeduction = view.findViewById(R.id.rdnSalaryDeduct);
        paypals = view.findViewById(R.id.rdnPaypal);
        totalAmount = view.findViewById(R.id.tvAmount);
        delivery = view.findViewById(R.id.btnDelivery);
        salary = view.findViewById(R.id.btnSalary);
        paypal = view.findViewById(R.id.btnPaypal);
        paymentType = view.findViewById(R.id.tvPaymentType);
        etCountPerson = view.findViewById(R.id.et_count_person);
        tvDateNow = view.findViewById(R.id.tv_date_now);
        tvTimeNow = view.findViewById(R.id.tv_time_now);

        constraintPaymentMethond = view.findViewById(R.id.constraint_PaymentMethod);
        constraintReservation = view.findViewById(R.id.constraint_Reservation);

        total = view.findViewById(R.id.tv_NewTotalAmount);
        type = view.findViewById(R.id.tv_NewPaymentMethod);

        mTabLayout = view.findViewById(R.id.tabLayout_PaymentMethod);
        mTabLayout.setElevation(0);
        mTabLayout.addTab(mTabLayout.newTab().setText("Payment Method"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Reservation"));

        cod = view.findViewById(R.id.cv_COD);
        sd = view.findViewById(R.id.cv_SalaryDeduction);
        pp = view.findViewById(R.id.cv_PayPal);
        btnPay = view.findViewById(R.id.btn_ContinuePay);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    constraintPaymentMethond.setVisibility(View.VISIBLE);
                    constraintReservation.setVisibility(View.GONE);
                } else {
                    constraintPaymentMethond.setVisibility(View.GONE);
                    constraintReservation.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = "CASH";
                cod.setBackgroundResource(R.drawable.cardview_bg_selected);
                sd.setBackgroundColor(Color.WHITE);
                pp.setBackgroundColor(Color.WHITE);
                type.setText(paymentMethod);
            }
        });

        sd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = "PAYROLL";
//                delivery.setBackgroundColor(Color.parseColor("#dce8d5"));
                cod.setBackgroundColor(Color.WHITE);
                sd.setBackgroundResource(R.drawable.cardview_bg_selected);
                pp.setBackgroundColor(Color.WHITE);
                type.setText(paymentMethod);
            }
        });

        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = "WALLET";
//                delivery.setBackgroundColor(Color.parseColor("#dce8d5"));
                cod.setBackgroundColor(Color.WHITE);
                sd.setBackgroundColor(Color.WHITE);
                pp.setBackgroundResource(R.drawable.cardview_bg_selected);
                type.setText(paymentMethod);
            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentMethod.equals("")) {
                    Toasty.warning(context, "Please select payment method.").show();
                } else {
                    getTime = String.valueOf(tvTimeNow.getText().toString());
                    getDate = String.valueOf(tvDateNow.getText().toString());
                    countPerson = Integer.parseInt(etCountPerson.getText().toString());

                    Syncer.saveSales(context, false,view, countPerson, getTime, getDate, paymentMethod);
                }
            }
        });

        tvDateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialogDate();
            }
        });

        tvTimeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialogTime();
            }
        });

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String totalGross = formatter.format(Double.parseDouble(amount));
        total.setText(String.format("₱%s", totalGross));

        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = "CASH";
                delivery.setBackgroundColor(Color.parseColor("#dce8d5"));
                salary.setBackgroundResource(android.R.drawable.btn_default);
                paypal.setBackgroundResource(android.R.drawable.btn_default);
                paymentType.setText(paymentMethod);
            }
        });

        salary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = "PAYROLL";
                salary.setBackgroundColor(Color.parseColor("#dce8d5"));
                paypal.setBackgroundResource(android.R.drawable.btn_default);
                delivery.setBackgroundResource(android.R.drawable.btn_default);
                paymentType.setText(paymentMethod);
            }
        });

        paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = "WALLET";
                paypal.setBackgroundColor(Color.parseColor("#dce8d5"));
                salary.setBackgroundResource(android.R.drawable.btn_default);
                delivery.setBackgroundResource(android.R.drawable.btn_default);
                paymentType.setText(paymentMethod);
            }
        });

        paymentType.setText(paymentMethod);

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentMethod.equals("")) {
                    Toasty.warning(context, "Please select payment method", Toast.LENGTH_SHORT).show();
                } else {
                    getTime = String.valueOf(tvTimeNow.getText().toString());
                    getDate = String.valueOf(tvDateNow.getText().toString());
                    countPerson = Integer.parseInt(etCountPerson.getText().toString());

                    Syncer.saveSales(context, false,view, countPerson, getTime, getDate, paymentMethod);
                }
            }
        });

        setDefaultDate();
    }


    private void setDefaultDate() {

        Calendar mCurrentTime = Calendar.getInstance();
        year = mCurrentTime.get(Calendar.YEAR);
        month = mCurrentTime.get(Calendar.MONTH);
        day = mCurrentTime.get(Calendar.DATE);
        hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mCurrentTime.get(Calendar.MINUTE);

        newDate.set(year, month, day);
        tvTimeNow.setText(DateTimeHandler.convert24to12(hour + ":" + minute));
        tvDateNow.setText(DateTimeHandler.convertDisplayDate(newDate.getTime()));
    }

    private void setDialogTime() {

        Calendar mCurrentTime = Calendar.getInstance();
        hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mCurrentTime.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                getHour = hourOfDay;
                getMinute = minute;
                tvTimeNow.setText(DateTimeHandler.convert24to12(getHour + ":" + getMinute));
            }

        }, hour, minute, false);
        timePickerDialog.show();
    }

    private void setDialogDate() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newDate.set(year, monthOfYear, dayOfMonth);
                tvDateNow.setText(DateTimeHandler.convertDisplayDate(newDate.getTime()));
            }

        }, year, month, day);
        datePickerDialog.show();
    }

    private void setDefaultMethod() {
        paymentMethod = "Salary Deduction";
//                delivery.setBackgroundColor(Color.parseColor("#dce8d5"));
        cod.setBackgroundColor(Color.WHITE);
        sd.setBackgroundResource(R.drawable.cardview_bg_selected);
        pp.setBackgroundColor(Color.WHITE);
        type.setText(paymentMethod);

    }
}
