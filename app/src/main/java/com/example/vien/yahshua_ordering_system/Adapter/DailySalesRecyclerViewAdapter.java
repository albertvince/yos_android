package com.example.vien.yahshua_ordering_system.Adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Fragments.DailySalesFragment;
import com.example.vien.yahshua_ordering_system.Fragments.ProductsFragment;
import com.example.vien.yahshua_ordering_system.Fragments.SearchFragment;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.DailySales;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.util.ArrayList;

public class DailySalesRecyclerViewAdapter extends RecyclerView.Adapter<DailySalesRecyclerViewAdapter.ViewHolder> {

    private ArrayList<DailySales> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private Fragment fragment;

    // data is passed into the constructor
    public DailySalesRecyclerViewAdapter(Context context, Fragment fragment, ArrayList<DailySales> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.fragment = fragment;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listrow_daily_sales_rc, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final DailySales dailySales = mData.get(position);
        holder.tvDate.setText(DateTimeHandler.convertDisplayDate(dailySales.getDateEnd()));
//        holder.tvSnack.setText(dailySales.getInventory().getSubDescription());
//        holder.tvLunch.setText(dailySales.getInventory().getSubDescription());
        holder.tvCompany.setText(dailySales.getCompanyName());

        holder.btnShopHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DailySalesFragment) fragment).gotoProduct(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<DailySales> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        CardView cardView;
        TextView tvDate;
        TextView tvLunch;
        TextView tvSnack, tvCompany;
        Button   btnShopHere;


        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv_daily_sales);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvLunch = itemView.findViewById(R.id.tv_lunch);
            tvSnack = itemView.findViewById(R.id.tv_snack);
            tvCompany = itemView.findViewById(R.id.tv_company);
            btnShopHere = itemView.findViewById(R.id.btn_shop_here);


            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    // convenience method for getting data at click position
    public DailySales getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    }
