package com.example.vien.yahshua_ordering_system.DialogFragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Model.Inventory;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.text.DecimalFormat;

public class ViewItemsDialog extends DialogFragment {

    private Context context;
    private Button btnView;
    private String Name, calledBy;
    private int quantityLeft;
    private double priceAmount, itemTotalGross, itemQuantity, itemPrice;
    private String Image, itemStatus;
    private String Description;
    private TextView name, description, price, stock, tvName, tvPrice, tvQuantity, tvTotal, tvStatus;
    private LinearLayout layoutOrderDetails, layoutItemsDetails, layoutDescription, layoutStatus;
    private ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.view_items_dialog, container, false);
        context = view.getContext();

        if (getArguments() != null)
        {
            calledBy = getArguments().getString("WHO_CALL");

            if(calledBy == "ORDER_DETAILS"){
                Name = getArguments().getString("itemName");
                itemPrice = getArguments().getDouble("itemPrice");
                Image = getArguments().getString("itemImage");
                itemTotalGross = getArguments().getDouble("itemTotal");
                itemQuantity = getArguments().getDouble("itemQuantity");
                itemStatus = getArguments().getString("itemStatus");
            } else {
                Name = getArguments().getString("itemName");
                priceAmount = getArguments().getDouble("itemPrice");
                Image = getArguments().getString("itemImage");
                Description = getArguments().getString("itemDescription");
                quantityLeft = getArguments().getInt("itemStock");
            }
        }

        initializeUI(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);
    }

    private void showDetails(boolean show){

        layoutItemsDetails.setVisibility(show ? View.GONE : View.VISIBLE);
        layoutOrderDetails.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showStatus(boolean show){
        layoutStatus.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showDescription(boolean show){
        layoutDescription.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void initializeUI(View view){

        btnView = view.findViewById(R.id.btnClose);
        name = view.findViewById(R.id.tvItemName);
        description = view.findViewById(R.id.tvDescription);
        price = view.findViewById(R.id.tvPrice);
        imageView = view.findViewById(R.id.ivDialog);
        stock = view.findViewById(R.id.tv_stock);
        layoutItemsDetails = view.findViewById(R.id.layout_item_details);
        layoutStatus = view.findViewById(R.id.layout_status);
        layoutDescription = view.findViewById(R.id.layout_description);

        tvName = view.findViewById(R.id.tv_name);
        tvPrice = view.findViewById(R.id.tv_price);
        tvQuantity = view.findViewById(R.id.tv_quantity);
        tvStatus = view.findViewById(R.id.tv_status);
        tvTotal = view.findViewById(R.id.tv_total);
        layoutOrderDetails = view.findViewById(R.id.layout_order_details);

        viewDetails();

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        showDetails(calledBy == "ORDER_DETAILS");
        showStatus(itemStatus != "");
        showDescription(Description != "");

    }

    private void viewDetails() {

        DecimalFormat formatter = new DecimalFormat("####.00");
        DecimalFormat quantityFormat = new DecimalFormat("####");
        String priceLabel = formatter.format(priceAmount);
        String itemPriceLabel = formatter.format(itemPrice);
        String quantityLabel = quantityFormat.format(itemQuantity);
        String totalGrossLabel = formatter.format(itemTotalGross);

        name.setText(Name);
        description.setText( Description);
        price.setText(addZeroes(priceLabel));
        stock.setText(String.valueOf(quantityLeft));
        ifImageAvailable(Image);

        tvName.setText(Name);
        tvStatus.setText(itemStatus);
        tvQuantity.setText(quantityLabel);
        tvPrice.setText(itemPriceLabel);
        tvTotal.setText(totalGrossLabel);

    }

    //Display list of image using Glide
    private void displayImage(String image)
    {
        RequestOptions myOption = new RequestOptions()
                .centerInside();

        Glide.with(context)
                .load(image.replace("//media","/media"))
                .apply(myOption)
                .into(imageView);
    }

    private void ifImageAvailable(String image){

        if(image.equals(HttpClientProvider.getBaseURL()+"media/default_inventory.jpg") || !image.contains("/media")){
            Debugger.logD("Default image");
        } else {
            displayImage(image);
        }
    }

    private String addZeroes(String val){
        if (val.equals(".00")) val = "00" + val;
        return val;
    }

    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        setDialogOnTop();
        super.onResume();
    }

    private void setDialogOnTop()
    {
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.x = 200;
        getDialog().getWindow().setAttributes(p);
    }

    public int getTheme()
    {
        return R.style.full_screen_dialog;
    }
}
