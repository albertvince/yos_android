package com.example.vien.yahshua_ordering_system.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.vien.yahshua_ordering_system.R;

public class PurchaseActivity extends AppCompatActivity {

    // Context
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        context = this;

        initializeUI();
    }

    // Initialize component from a layout file
    private void initializeUI() {
    }

}
