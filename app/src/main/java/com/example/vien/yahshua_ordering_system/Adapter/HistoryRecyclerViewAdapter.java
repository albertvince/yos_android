package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Interfaces.RecyclerViewClick;
import com.example.vien.yahshua_ordering_system.Model.Invoice;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.DateTimeHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class HistoryRecyclerViewAdapter extends RecyclerView.Adapter<HistoryRecyclerViewAdapter.MyViewHolder>{

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Invoice> invoiceArrayList;

    private RecyclerViewClick listener;

    public HistoryRecyclerViewAdapter(Context context, ArrayList<Invoice> invoiceArrayList, RecyclerViewClick listener) {
        this.context = context;
        this.invoiceArrayList = invoiceArrayList;
        this.listener = listener;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.listrow_history, viewGroup, false);
        final MyViewHolder myViewHolder = new MyViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, myViewHolder.getAdapterPosition());
            }
        });

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final Invoice invoice = invoiceArrayList.get(i);

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String amountLabel = formatter.format(invoice.getTotalGross());

        myViewHolder.tvRefNo.setText("Ref No: " + invoice.getReferenceNo());
        myViewHolder.tvDate.setText(DateTimeHandler.convertDisplayDate(invoice.getDateCreated()) + " | " + DateTimeHandler.formatTime(invoice.getDateCreated()));
        myViewHolder.tvTotal.setText(String.valueOf("₱"+amountLabel));
    }

    @Override
    public int getItemCount() {
        return invoiceArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvRefNo, tvDate, tvTotal;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRefNo = itemView.findViewById(R.id.tv_HistoryRefNo);
            tvDate = itemView.findViewById(R.id.tv_HistoryDate);
            tvTotal = itemView.findViewById(R.id.tv_HistoryTotalTotal);
        }
    }
}
