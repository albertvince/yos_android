package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Fragments.StoreProductFragment;
import com.example.vien.yahshua_ordering_system.Model.Inventory;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.text.DecimalFormat;
import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class StoreRecyclerViewAdapter extends RecyclerView.Adapter<StoreRecyclerViewAdapter.ViewHolder> {
    private ArrayList<Inventory> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private Fragment fragment;

    public StoreRecyclerViewAdapter(Context context, Fragment fragment, ArrayList<Inventory> data){
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listrow_home, parent, false);
        context = parent.getContext();

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final  Inventory inventory = mData.get(position);

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        DecimalFormat formatQuantity= new DecimalFormat("#,###");
        String priceLabel = formatter.format(inventory.getPrice());
        String totalQuantity = formatQuantity.format(inventory.getQuantity());

        holder.name.setText(inventory.getName());
        holder.price.setText(String.format("₱%s",addZeroes(priceLabel)));
//        holder.description.setText(inventory.getDescription());
//        holder.tvSubDescription.setText(inventory.getSubDescription());
        holder.quantity.setText(totalQuantity);
        holder.company.setText(inventory.getCompanyName());

//        if(inventory.getQuantityLeft() > 0){
//            holder.quantityLeft.setText(String.valueOf("Stock Left: " + inventory.getQuantityLeft()));
//        }else {
//            holder.quantityLeft.setText("");
//        }
        ifImageAvailable(inventory, holder);

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inventory.getQuantity() <= 0) {
                    Toasty.warning(context, "Zero quantity", Toast.LENGTH_SHORT).show();
                } else {
                    inventory.addQuantity(-1);
                    notifyDataSetChanged();
                    ((StoreProductFragment) fragment).addItemToCart(position);
                    ((StoreProductFragment) fragment).countItem();
                }

            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inventory.addQuantity(1);
                notifyDataSetChanged();
                ((StoreProductFragment) fragment).addItemToCart(position);
                ((StoreProductFragment) fragment).countItem();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public Inventory getItem(int id) {
        return mData.get(id);
    }

    public ArrayList<Inventory> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        private TextView price, name, description, tvSubDescription, quantity, company,quantityLeft;
        private Button add, minus;
        private ImageView imageView;
        private CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.tvItemPrice);
            name = itemView.findViewById(R.id.tvItemName);
            imageView = itemView.findViewById(R.id.ivHomeItemImage);
            description= itemView.findViewById(R.id.tvDescription);
            tvSubDescription = itemView.findViewById(R.id.tvSubDescription);
            minus = itemView.findViewById(R.id.btnDeductQuantity);
            add = itemView.findViewById(R.id.btnAddQuantity);
            quantity = itemView.findViewById(R.id.tvQuantity);
            company = itemView.findViewById(R.id.tvCompany);
            quantityLeft = itemView.findViewById(R.id.tv_quantity_left);
            cardView = itemView.findViewById(R.id.cv_store);

//            cardView.setOnClickListener(this);
//            cardView.setOnCreateContextMenuListener(this);

            imageView.setOnClickListener(this);
            imageView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }

    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    //Display list of image using Glide
    private void displayImage(Inventory inventory, ViewHolder holder)
    {
        RequestOptions myOption = new RequestOptions()
                .centerInside();

        Glide.with(fragment)
                .load(inventory.getImageURL().replace("//media","/media"))
                .apply(myOption)
                .into(holder.imageView);
    }

    private void ifImageAvailable(Inventory inventory, ViewHolder holder){

        if (inventory.getImageURL().equals(HttpClientProvider.getBaseURL() + "media/default_inventory.jpg") || !inventory.getImageURL().contains("/media")) {
            Debugger.logD("Default image");
        } else {
            displayImage(inventory, holder);
        }
    }

    private String addZeroes(String val){
        if (val.equals(".00")) val = "00" + val;
        return val;
    }
}
