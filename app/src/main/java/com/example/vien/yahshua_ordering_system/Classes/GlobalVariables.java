package com.example.vien.yahshua_ordering_system.Classes;

import android.content.Context;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Model.CompanyType;

import java.util.ArrayList;

public class GlobalVariables {

    public static int itemCount = 0;
    public static boolean isRequestSuccess = false;
    public static String email = "";
    public static ArrayList<CompanyType> companyTypeArrayList = new ArrayList<>();

    public static void ShowToast(Context v, String message) {
        Toast toast = Toast.makeText(v, "" + message, Toast.LENGTH_LONG);
        toast.show();
    }
}
