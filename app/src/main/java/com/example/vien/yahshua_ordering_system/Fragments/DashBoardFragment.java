package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.vien.yahshua_ordering_system.Activities.CompanyActivity;
import com.example.vien.yahshua_ordering_system.Activities.DashboardResultActivity;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.R;

public class DashBoardFragment extends Fragment {

    // Views and Context
    private View view;
    private  Context context;

    //Widgets
    private Button btnFood, btnHotel, btnBank;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dash_board, container, false);

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = view.getContext();

        initializeUI(view);
    }

    private void initializeUI(View view){

        btnBank= view.findViewById(R.id.btn_bank);
        btnFood= view.findViewById(R.id.btn_food);
        btnHotel= view.findViewById(R.id.btn_hotel);

        ((MainActivity) context).tabLayout.setVisibility(View.GONE);

        btnFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(context, CompanyActivity.class);
//                intent.setClass(context, DashboardResultActivity.class);
                intent.putExtra("TITLE","Food");
                startActivity(intent);
            }
        });

    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }
}
