package com.example.vien.yahshua_ordering_system.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.example.vien.yahshua_ordering_system.Adapter.AccountViewPagerAdapter;
import com.example.vien.yahshua_ordering_system.Fragments.CompanyFragment;
import com.example.vien.yahshua_ordering_system.Fragments.ShopFragment;
import com.example.vien.yahshua_ordering_system.Fragments.StoreProductFragment;
import com.example.vien.yahshua_ordering_system.Model.Checkout;
import com.example.vien.yahshua_ordering_system.Model.Company;
import com.example.vien.yahshua_ordering_system.Model.CompanyType;
import com.example.vien.yahshua_ordering_system.R;

public class StoreAndItemActivity extends AppCompatActivity {

    //Context
    private Context context;

    //Widgets
    private ViewPager viewPager;
    private AccountViewPagerAdapter accountViewPagerAdapter;
    private TabLayout tabLayout;
    private FragmentManager manager;

    //Data
    private CompanyType companyType;
    private Company company;
    private boolean fromCheckout = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_and_item);
        context = this;

        getSupportActionBar().setElevation(0);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//
//        manager = getSupportFragmentManager();
//        manager.popBackStack();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {

            fromCheckout = getIntent().getBooleanExtra("FROM_SHOP_FRAGMENT", false);

            if(fromCheckout){
                company = getIntent().getParcelableExtra("COMPANY");
                initializeUI();
                populateViewPager();
                tabLayout.setVisibility(View.GONE);
            } else{
                companyType = getIntent().getParcelableExtra("COMPANYTYPE");
                company = getIntent().getParcelableExtra("COMPANY");
                setTitle(companyType.getName());
                initializeUI();
                populateViewPager();
            }
       }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Checkout.getTotalItemCount(context) != 0 && !fromCheckout) {
           viewPager.setCurrentItem(1);
        }else{
            viewPager.setCurrentItem(0);
        }
    }

    private void initializeUI(){
        viewPager = findViewById(R.id.vp_store_and_item);
        tabLayout = findViewById(R.id.tl_store_item);
    }

    //Add fragments
    private void populateViewPager() {
        accountViewPagerAdapter = new AccountViewPagerAdapter(getSupportFragmentManager());
        if(fromCheckout){
            accountViewPagerAdapter.addFragment(StoreProductFragment.newInstance(null, company ), "Food");
        } else{
            accountViewPagerAdapter.addFragment(CompanyFragment.newInstance(companyType.getName(), companyType.getUuid()), "Store");
            accountViewPagerAdapter.addFragment(StoreProductFragment.newInstance(companyType, company ), "Food");
        }
        viewPager.setAdapter(accountViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void openStoreProductFragment(){
        viewPager.setCurrentItem(1);
    }
}
