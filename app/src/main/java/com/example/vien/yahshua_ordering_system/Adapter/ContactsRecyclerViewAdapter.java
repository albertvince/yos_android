package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Interfaces.RecyclerViewClick;
import com.example.vien.yahshua_ordering_system.Model.ContactsModel;
import com.example.vien.yahshua_ordering_system.R;

import java.util.ArrayList;

public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<ContactsModel> contacts = new ArrayList<>();
    private ArrayList<ContactsModel> searchContacts = new ArrayList<>();
    private RecyclerViewClick listener;

    public ContactsRecyclerViewAdapter(Context context, ArrayList<ContactsModel> contacts) {
        this.context = context;
        this.contacts = contacts;
        this.layoutInflater = LayoutInflater.from(context);

        searchContacts = new ArrayList<>(contacts);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.listrow_contacts, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        ContactsModel contactsModel = contacts.get(i);

        displayImage(contactsModel, myViewHolder);
        myViewHolder.tvName.setText(contactsModel.getFullName());
        myViewHolder.tvNumber.setText(contactsModel.getNumber());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    @Override
    public Filter getFilter() {
        return contactFilter;
    }

    private Filter contactFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<ContactsModel> filteredContacts = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredContacts.addAll(searchContacts);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (ContactsModel contactsModel : searchContacts){
                    if (contactsModel.getFullName().toLowerCase().contains(filterPattern) || contactsModel.getNumber().toLowerCase().contains(filterPattern)) {
                        filteredContacts.add(contactsModel);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredContacts;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            contacts.clear();
            contacts.addAll((ArrayList)results.values);
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView ivImage;
        TextView tvName, tvNumber;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.iv_ContactsImage);
            tvName = itemView.findViewById(R.id.tv_ContactsFullName);
            tvNumber = itemView.findViewById(R.id.tv_ContactsNumber);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onItemClick(v, getAdapterPosition());
                    }
                }
            });
        }
    }

    public ArrayList<ContactsModel> getData() {
        return contacts;
    }

    public ContactsModel getItem(int id) {
        return contacts.get(id);
    }

    public void setOnClick(RecyclerViewClick recyclerViewClick) {
        this.listener = recyclerViewClick;
    }

    private void displayImage(ContactsModel contactsModel, MyViewHolder myViewHolder)
    {
        RequestOptions myOption = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.account_profile);
        Glide.with(context)
                .load(contactsModel.getImage())
                .apply(myOption)
                .into(myViewHolder.ivImage);
    }

}
