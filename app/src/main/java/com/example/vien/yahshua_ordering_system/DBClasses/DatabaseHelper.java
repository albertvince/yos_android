package com.example.vien.yahshua_ordering_system.DBClasses;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vien.yahshua_ordering_system.Utils.Debugger;

public class DatabaseHelper extends SQLiteOpenHelper
{
    public static final String DATABASE_NAME = "db_ordering";
    private static final int DATABASE_VERSION = 3;
    private String cartTable, userTable;

    private final String dropTable = "DROP TABLE session";

    // Tables need to be backed up
    private void initializeTables()
    {
        cartTable="CREATE TABLE cart(";
        cartTable += " id INTEGER PRIMARY KEY AUTOINCREMENT,";
        cartTable += " name TEXT,";
        cartTable += " price NUMERIC DEFAULT 0,";
        cartTable += " quantity NUMERIC DEFAULT 0,";
        cartTable += " total NUMERIC DEFAULT 0,";
        cartTable += " image TEXT,";
        cartTable += " date DATE,";
        cartTable += " uuId TEXT,";
        cartTable += " category TEXT,";
        cartTable += " is_checkout INT DEFAULT 0)";

        userTable="CREATE TABLE user(";
        userTable += " id INTEGER PRIMARY KEY AUTOINCREMENT,";
        userTable += " first_name TEXT,";
        userTable += " last_name TEXT,";
        userTable += " e_mail TEXT,";
        userTable += " token TEXT)";
    }

    DatabaseHelper(Context context, SQLiteDatabase.CursorFactory factory)
    {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try
            {
                initializeTables();

                // Transactional Tables
                db.execSQL(cartTable);
                db.execSQL(userTable);

            } catch (SQLException err){
                Debugger.logD(err.toString());
            }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int _oldVersion, int _newVersion)
    {
        //New Tables
        initializeTables();
        Debugger.logD(_newVersion + " Version");
        if (_newVersion == 3){
            Debugger.logD("Database Upgrade Version 3");
            db.execSQL(userTable);
        }
        Debugger.logD("Upgrading from version " +_oldVersion + " to " +_newVersion + ", which will destroy all old data");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}
