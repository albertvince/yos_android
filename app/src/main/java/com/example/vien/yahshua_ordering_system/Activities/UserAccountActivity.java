package com.example.vien.yahshua_ordering_system.Activities;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien.yahshua_ordering_system.Adapter.UserAccountAdapter;
import com.example.vien.yahshua_ordering_system.DialogFragments.FilterDateDialog;
import com.example.vien.yahshua_ordering_system.DialogFragments.LoginDialog;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.UserAccount;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;


public class UserAccountActivity extends AppCompatActivity {

    // Context
    private Context context;

    // Widgets
    private ListView lvAccount;
    private Button btnLogin, btnLogut;

    //Adapter and data
    private UserAccountAdapter userAccountAdapter;
    private ArrayList<UserAccount> userAccountArrayList = new ArrayList<>();
    private UserAccount selectedUserAccount;
    private UserSession userSession = new UserSession();
    private ArrayList<String> userArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);
        context = this;

        initializeUI();
        readRecords();
    }

    private void initializeUI()
    {
        lvAccount = findViewById(R.id.lv_account);
        btnLogin = findViewById(R.id.btn_login);
        btnLogut = findViewById(R.id.btn_signup);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnLogut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });

        lvAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectedUserAccount = userAccountAdapter.getItem(position);

                    openLoginDialog(selectedUserAccount.getEmail(), selectedUserAccount.getFirstName(), selectedUserAccount.getLastName(), selectedUserAccount.getToken());
            }
        });

       lvAccount.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
           @Override
           public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
               selectedUserAccount = userAccountAdapter.getItem(position);
//               userAccountAdapter.confirmCancel(selectedUserAccount);
               return true;

           }
       });

        userAccountAdapter = new UserAccountAdapter(context, R.layout.listrow_account, userAccountArrayList);
        lvAccount.setAdapter(userAccountAdapter);
    }

    public void readRecords(){

        userAccountArrayList = UserAccount.read(context, "");
        userAccountAdapter.clear();
        userAccountAdapter.addAll(userAccountArrayList);
        userAccountAdapter.notifyDataSetChanged();

        if(userAccountArrayList.size() == 0 ){
          btnLogin.performClick();
        }
    }

    public void openLoginDialog(String email, String firstName,String lastName, String token){

        Bundle bundle = new Bundle();
        bundle.putString("EMAIL", email);
        bundle.putString("FIRST_NAME", firstName);
        bundle.putString("LAST_NAME", lastName);
        bundle.putString("TOKEN", token);

        FragmentManager fm = getFragmentManager();
        LoginDialog loginDialog = new LoginDialog();
        loginDialog.setArguments(bundle);
        loginDialog.show(fm, "ITEM");
    }
}
