package com.example.vien.yahshua_ordering_system.Activities;

import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Adapter.InvoiceProductAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.DialogFragments.ViewItemsDialog;
import com.example.vien.yahshua_ordering_system.Model.Invoice;
import com.example.vien.yahshua_ordering_system.Model.InvoiceProduct;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Converter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class OrderDetailsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    //Context
    private Context context;

    //Widgets
    private ListView listView;
    private SwipeRefreshLayout swipeLayout;
    private TextView tvTotal,tvShipping, tvSubtotal;
    private LinearLayout  linearLayoutBreakDown;
    private View viewEmptyListIndicator;

    //Data
    private double totalOrder;

    //Adapters
    private InvoiceProductAdapter invoiceProductAdapter;

    private Invoice invoice;
    private InvoiceProduct selectedInvoiceProduct;
    private ArrayList<InvoiceProduct> invoiceProductsArrayList = new ArrayList<>();

//    private static String INVOICE = "arg1";


    public OrderDetailsActivity(){

    }

//    public static OrderDetailsActivity newInstance(String param1) {
//        OrderDetailsActivity activity = new OrderDetailsActivity();
//        Bundle args = new Bundle();
//        args.putString(INVOICE, param1);
//        return activity;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details_activity);
        context = this;

    }

    @Override
    protected void onStart() {
        super.onStart();
        setTitle("Purchase Details");

        if(getIntent() != null) {
            invoice = getIntent().getParcelableExtra("INVOICE");
        }

        initializeUI();
        fetchInvoiceProducts();
    }

    private void initializeUI()
    {
        listView = findViewById(R.id.lvOrderDetails);
        tvTotal = findViewById(R.id.tvTotalOrder);
        tvShipping = findViewById(R.id.tvShippingFee);
        tvSubtotal = findViewById(R.id.tvSubTotal);
        linearLayoutBreakDown = findViewById(R.id.linearLayout_breakdown);
        viewEmptyListIndicator = findViewById(R.id.viewEmptyListIndicator);

        invoiceProductAdapter = new InvoiceProductAdapter(context, invoiceProductsArrayList);
        listView.setAdapter(invoiceProductAdapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getChildAt(0) != null) {
                    swipeLayout.setEnabled(listView.getFirstVisiblePosition() == 0 && listView.getChildAt(0).getTop() == 0);
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedInvoiceProduct = invoiceProductAdapter.getItem(position);
                String ITEM_STATUS;

                if(selectedInvoiceProduct.isVoid()){
                    ITEM_STATUS = String.valueOf(Converter.BooleanIsVoid(true));
                }else if(selectedInvoiceProduct.isConfirmed()){
                    ITEM_STATUS = String.valueOf(Converter.BooleanIsConfirmed(true));
                } else {
                    ITEM_STATUS ="";
                }

                Bundle bundle = new Bundle();
                bundle.putString("itemName", selectedInvoiceProduct.getInventory().getName());
                bundle.putString("itemImage", selectedInvoiceProduct.getInventory().getImageURL());
                bundle.putDouble("itemQuantity", selectedInvoiceProduct.getQuantity());
                bundle.putDouble("itemPrice", selectedInvoiceProduct.getPrice());
                bundle.putDouble("itemTotal", selectedInvoiceProduct.getTotalGross());
                bundle.putString("itemStatus", ITEM_STATUS);
//                bundle.putDouble("itemStatus", selectedInvoiceProduct.isConfirmed());
                bundle.putString("WHO_CALL","ORDER_DETAILS");
//                bundle.putInt("itemStock", selectedInvoiceProduct.getQuantityLeft());

                FragmentManager fm = getFragmentManager();
                ViewItemsDialog viewItemsDialog = new ViewItemsDialog();
                viewItemsDialog.setArguments(bundle);
                viewItemsDialog.show(fm, "DETAILS");

            }
        });

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));

        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));
    }

    private void readRecords(){

        invoiceProductAdapter.clear();
        invoiceProductAdapter.addAll(invoiceProductsArrayList);
        invoiceProductAdapter.notifyDataSetChanged();
        totalOrder();

        showEmptyView(invoiceProductsArrayList.size() < 1 );
    }

    private void showEmptyView(boolean show)
    {
        viewEmptyListIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        listView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void fetchInvoiceProducts()
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("invoice", invoice.getUuid());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "read_invoice_products/", stringEntity, new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        super.onSuccess(statusCode, headers, response);
                        invoiceProductsArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<InvoiceProduct>>(){}.getType());
                        readRecords();
                        swipeLayout.setRefreshing(false);
                    } catch (Exception err)
                    {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.error(context, responseString);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }

                @Override
                public void onFinish() {
                    swipeLayout.setRefreshing(false);
                }
            });




        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    private void totalOrder(){

        if(totalOrder < 1)
            for(InvoiceProduct invoiceProduct : this.invoiceProductsArrayList){
            totalOrder += invoiceProduct.getPrice() * invoiceProduct.getQuantity();
        }

        DecimalFormat formatter = new DecimalFormat("#,###.00");
        String totalLabel = formatter.format(totalOrder);
        String subTotalLabel = formatter.format(totalOrder);

        tvSubtotal.setText("₱" + String.valueOf(subTotalLabel));
        tvTotal.setText("₱" + String.valueOf(totalLabel));

    }


    @Override
    public void onRefresh() {
        fetchInvoiceProducts();
    }

    //Return last Fragment/Activity
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //End

}
