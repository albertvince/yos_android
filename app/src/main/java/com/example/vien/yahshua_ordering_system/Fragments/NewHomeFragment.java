package com.example.vien.yahshua_ordering_system.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Activities.AccountActivity;
import com.example.vien.yahshua_ordering_system.Activities.NotificationActivity;
import com.example.vien.yahshua_ordering_system.Activities.StoreAndItemActivity;
import com.example.vien.yahshua_ordering_system.Adapter.CompanyTypeAdapter;
import com.example.vien.yahshua_ordering_system.Classes.GlobalVariables;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.DialogFragments.PayDialog;
import com.example.vien.yahshua_ordering_system.MainActivity;
import com.example.vien.yahshua_ordering_system.Model.CompanyType;
import com.example.vien.yahshua_ordering_system.Model.ContactsModel;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.example.vien.yahshua_ordering_system.Utils.UserSession;
import com.example.vien.yahshua_ordering_system.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewHomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, CompanyTypeAdapter.ItemCompanyTypeClickListener {

    private Context context;
    private View view;

    private TextView tvGreeting, tvFullName, tvBalance, tvProfileStrengthFullName, tvProfileStrengthFullName2, tvAllDone, tvViewCompletedTasks;
    private ImageView ivProfileStrengthImage, ivPay, ivRequest, ivTopUp;
    private RecyclerView recyclerViewCategory;
    private SwipeRefreshLayout swipeLayout;
    private ProgressBar progressBarBalance;

    private CompanyTypeAdapter companyTypeAdapter;
    private ArrayList<String> images = new ArrayList<>();

    private String amountLabel = "";
    private boolean isEmail = false;

    public NewHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_new_home, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);

        context = getContext();
        initializeUI();

        Debugger.logD(GlobalVariables.companyTypeArrayList.size() + " list");

        fetchFromOnline(GlobalVariables.companyTypeArrayList.size() == 0);

    }

    private void initializeUI() {
        ivPay = view.findViewById(R.id.iv_HomePay);
        ivRequest = view.findViewById(R.id.iv_HomeRequest);
        ivTopUp = view.findViewById(R.id.iv_HomeTopUp);
        tvAllDone = view.findViewById(R.id.tv_HomeAllDone);
        tvViewCompletedTasks = view.findViewById(R.id.tv_HomeViewCompletedTasks);
        progressBarBalance = view.findViewById(R.id.progressBar_HomeBalance);
        tvBalance = view.findViewById(R.id.tv_HomeBalance);
        recyclerViewCategory = view.findViewById(R.id.recyclerView_HomeCategory);
        tvProfileStrengthFullName = view.findViewById(R.id.tv_HomeProfileStrengthFullName);
        tvProfileStrengthFullName2 = view.findViewById(R.id.tv_HomeProfileStrengthFullName2);
        ivProfileStrengthImage = view.findViewById(R.id.iv_HomeProfileStrengthImage);

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        ((MainActivity) context).tabLayout.setVisibility(View.GONE);

        ivPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                PayDialog payDialog = PayDialog.newInstance(amountLabel);
                payDialog.show(fm, "PayDialogTag");
            }
        });

        ivRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "Request clicked.").show();
            }
        });

        ivTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "Top Up clicked.").show();
            }
        });

        tvAllDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "All done clicked.").show();
            }
        });

        tvViewCompletedTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "View completed tasks clicked.").show();
            }
        });
    }

    private void fetchFromOnline(boolean show){
        if(show){
            fetchCompanyTypes();
            getWalletBalance();
            loadProfileStrength();
        }else{
            readCompanyTypeRecords();
            getWalletBalance();
            loadProfileStrength();
        }
    }

    private void getWalletBalance() {
        try {
            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("invoice", invoice.getUuid());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "get_wallet_balance/", stringEntity, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    progressBarBalance.setVisibility(View.VISIBLE);
                    tvBalance.setVisibility(View.GONE);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        DecimalFormat formatter = new DecimalFormat("#,##0.00");
                        amountLabel = formatter.format(response.getInt("balance"));
                        tvBalance.setText("₱" + amountLabel);
                        progressBarBalance.setVisibility(View.GONE);
                        tvBalance.setVisibility(View.VISIBLE);

                    } catch (Exception err) {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    Toasty.error(context, "Can't connect to server").show();
                    swipeLayout.setRefreshing(false);
                    progressBarBalance.setVisibility(View.GONE);
                    tvBalance.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    Toasty.error(context, "Can't connect to server").show();
                    swipeLayout.setRefreshing(false);
                    progressBarBalance.setVisibility(View.GONE);
                    tvBalance.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
//                    Toasty.error(context, " " + responseString).show();
                    swipeLayout.setRefreshing(false);
                    progressBarBalance.setVisibility(View.GONE);
                    tvBalance.setVisibility(View.VISIBLE);
                }


            });
        } catch (Exception err) {
//            Toasty.error(context, err.toString()).show();
            progressBarBalance.setVisibility(View.GONE);
            tvBalance.setVisibility(View.VISIBLE);
        }
    }

    // Online functions
    public void fetchCompanyTypes() {
        try {
            HttpClientProvider.get(context, "read_company_types/", null, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {

                        GlobalVariables.companyTypeArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<CompanyType>>() {
                        }.getType());

                        CompanyType companyType = new CompanyType();
                        companyType.setName("All");
                        companyType.setUuid("");
                        GlobalVariables.companyTypeArrayList.add(0, companyType);

                        readCompanyTypeRecords();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Utility.showNoConnectionDialog(context);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.error(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                }

                @Override
                public void onFinish() {
                    swipeLayout.setRefreshing(false);
                }

            });


        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }
    }

    private void readCompanyTypeRecords() {

        images.add("all");
        images.add("restaurant");
        images.add("retail");
        images.add("gadgets");
        images.add("foods");
        images.add("services");
        images.add("hotel");
        images.add("default_item");
        images.add("shopping");

        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewCategory.setHasFixedSize(true);
        companyTypeAdapter = new CompanyTypeAdapter(context, NewHomeFragment.this, GlobalVariables.companyTypeArrayList, images);
        companyTypeAdapter.setClickListener(this);
        recyclerViewCategory.setAdapter(companyTypeAdapter);
        registerForContextMenu(recyclerViewCategory);
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }

    //Your new items in Action Bar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notification) {
            Intent notification = new Intent(context, NotificationActivity.class);
            startActivity(notification);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemCompanyTypeClick(View view, int position) {
        CompanyType selectedCompanyType = companyTypeAdapter.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putParcelable("COMPANYTYPE", selectedCompanyType);
        bundle.putBoolean("FROM_SHOP_FRAGMENT", false);

        Intent intent = new Intent();
        intent.setClass(context, StoreAndItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {

        if (!Utility.haveNetworkConnection(context)) {
            Utility.showNoConnectionDialog(context);
            swipeLayout.setRefreshing(false);
        }else{
            fetchCompanyTypes();
            getWalletBalance();
        }
    }

    private void loadProfileStrength() {
        tvProfileStrengthFullName.setText(UserSession.getFirstName(context) + " " + UserSession.getLastName(context));
        tvProfileStrengthFullName2.setText(UserSession.getFirstName(context) + " " + UserSession.getLastName(context) + "!");
        if (UserSession.getLocalImage(context).equals("")) {
            ivProfileStrengthImage.setImageResource(R.drawable.account_profile);
        } else {
            RequestOptions myOption = new RequestOptions().circleCrop();
            Glide.with(context).load(UserSession.getLocalImage(context)).apply(myOption).into(ivProfileStrengthImage);

        }
    }

}
