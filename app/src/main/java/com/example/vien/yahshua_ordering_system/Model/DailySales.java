package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class DailySales extends Model implements Parcelable {

    private String name;
    @SerializedName("date_start")
    private Date dateStart;
    @SerializedName("date_end")
    private Date dateEnd;
    private Inventory inventory;
    @SerializedName("company_name")
    private String companyName;

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    protected DailySales(Parcel in) {
        name = in.readString();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public static final Creator<DailySales> CREATOR = new Creator<DailySales>() {
        @Override
        public DailySales createFromParcel(Parcel in) {
            return new DailySales(in);
        }

        @Override
        public DailySales[] newArray(int size) {
            return new DailySales[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
