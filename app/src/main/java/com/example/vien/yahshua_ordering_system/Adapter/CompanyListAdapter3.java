package com.example.vien.yahshua_ordering_system.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Model.Company;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.util.ArrayList;

public class CompanyListAdapter3  extends RecyclerView.Adapter<CompanyListAdapter3.ViewHolder> {

    private ArrayList<Company> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    public CompanyListAdapter3(Context context,ArrayList<Company> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listrow_company, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Company company = mData.get(position);
        holder.tvName.setText(company.getCompanyName());
        ifImageAvailable(company, holder);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<Company> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        CardView cardView;
        TextView tvName;
        private ImageView imageView;


        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv_company_list);
            tvName = itemView.findViewById(R.id.tv_company_name);
            imageView = itemView.findViewById(R.id.img_company_logo);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick3(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    // convenience method for getting data at click position
    public Company getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick3(View view, int position);
    }

    //Display list of image using Glide
    private void displayImage(Company company, ViewHolder holder)
    {
        RequestOptions myOption = new RequestOptions()
                .centerInside();
        Glide.with(context)
                .load(company.getImageURL().replace("//media","/media"))
                .apply(myOption)
                .into(holder.imageView);

    }

    private void ifImageAvailable(Company company, ViewHolder holder){

        if (company.getImageURL().equals(HttpClientProvider.getBaseURL() + "media/default_inventory.jpg") || !company.getImageURL().contains("/media")) {
            Debugger.logD("Default image");
        } else {
            displayImage(company, holder);
        }
    }
}
