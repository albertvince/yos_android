package com.example.vien.yahshua_ordering_system.Model;

import android.content.Context;
import android.database.Cursor;

import com.example.vien.yahshua_ordering_system.DBClasses.DatabaseAdapter;
import com.example.vien.yahshua_ordering_system.Utils.Converter;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;

import java.util.ArrayList;

public class InvoiceProducts extends Checkout {

    public static ArrayList<InvoiceProducts> readForUpload(Context context)
    {
        ArrayList<InvoiceProducts> invoiceProductsArrayList = new ArrayList<InvoiceProducts>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery = "SELECT  uuId, quantity, price, category FROM cart WHERE is_checkout = 0 AND quantity > 0 ORDER BY id DESC";

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    InvoiceProducts invoiceProducts = new InvoiceProducts();

                    invoiceProducts.setItemQuantity(Converter.ObjectInt(cursor,"quantity"));
                    invoiceProducts.setUuId(cursor.getString(cursor.getColumnIndex("uuId")));
                    invoiceProducts.setCheckout(Converter.ObjectToBoolean(cursor, "is_checkout"));
                    invoiceProducts.setItemPrice(Converter.ObjectInt(cursor, "price"));
                    invoiceProducts.setCategory(Converter.ObjectToString(cursor, "category"));
                    invoiceProductsArrayList.add(invoiceProducts);

                    Debugger.printO(invoiceProducts);

                } while (cursor.moveToNext());

            }

            cursor.close();
            DatabaseAdapter.close();

            return invoiceProductsArrayList;

        } catch (Exception err) {

            return invoiceProductsArrayList;
        }
    }



}
