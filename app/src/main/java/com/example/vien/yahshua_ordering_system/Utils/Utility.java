package com.example.vien.yahshua_ordering_system.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.example.vien.yahshua_ordering_system.DialogFragments.ErrorDialogFragment;


public class Utility {

    public static void showNoConnectionDialog(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("No connection")
                .setMessage("Check your internet connection and try again.")
                .setPositiveButton("OK", null);
        builder.show();
    }

    public static boolean haveNetworkConnection(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo;

        if (connectivityManager != null) networkInfo = connectivityManager.getActiveNetworkInfo();
        else networkInfo = null;

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static void showError(FragmentManager fragmentManager, String errorMessage)
    {
        ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        Bundle bundle = new Bundle();

        if (errorMessage != null) bundle.putString("ERROR_MESSAGE", errorMessage);

        errorDialogFragment.setArguments(bundle);
        errorDialogFragment.show(fragmentManager, "ErrorDialog");
    }

}
