package com.example.vien.yahshua_ordering_system.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.vien.yahshua_ordering_system.Activities.StoreAndItemActivity;
import com.example.vien.yahshua_ordering_system.Adapter.CompanyListAdapter2;
import com.example.vien.yahshua_ordering_system.Adapter.CompanyListAdapter3;
import com.example.vien.yahshua_ordering_system.Adapter.CompanyTypeAdapter;
import com.example.vien.yahshua_ordering_system.Adapter.CompanyListAdapter;
import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.example.vien.yahshua_ordering_system.Model.Company;
import com.example.vien.yahshua_ordering_system.Model.CompanyType;
import com.example.vien.yahshua_ordering_system.R;
import com.example.vien.yahshua_ordering_system.Utils.Debugger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.navigation.Navigation;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class ShopFragment extends Fragment implements CompanyListAdapter.ItemClickListener, CompanyListAdapter2.ItemClickListener, CompanyListAdapter3.ItemClickListener, CompanyTypeAdapter.ItemCompanyTypeClickListener, SwipeRefreshLayout.OnRefreshListener {

    // Views and Context
    private Context context;
    private View view;

    //Widgets
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView rcCompanyList, rcCategory, rcCompanyTyp2, rcCompanyTyp3;
    private CompanyListAdapter companyListAdapter1;
    private CompanyListAdapter2 companyListAdapter2;
    private CompanyListAdapter3 companyListAdapter3;
    CompanyTypeAdapter companyTypeAdapter;
    private LinearLayout linearLayout;
    public TabLayout tabLayout;
    private CardView cardView;
    private TextView tvCompanyTypes1, tvCompanyTypes2, tvCompanyTypes3;
    //Data
    private ArrayList<CompanyType> companyTypeArrayList = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private FragmentManager manager;

    private Company selectedCompany;
    private CompanyType selectedCompanyType;

    public ShopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shop, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();

        getActivity().setTitle("Shop");

        initializeUI(view);
        fetchCompanyTypes();
    }

    private void initializeUI(View view) {

        rcCompanyList = view.findViewById(R.id.rc_company_list);
        rcCategory = view.findViewById(R.id.rc_category);
        rcCompanyTyp2 = view.findViewById(R.id.rc_company_type_list2);
        rcCompanyTyp3 = view.findViewById(R.id.rc_company_type_list3);
        tvCompanyTypes1 = view.findViewById(R.id.tv_company_types1);
        tvCompanyTypes2 = view.findViewById(R.id.tv_company_types2);
        tvCompanyTypes3 = view.findViewById(R.id.tv_company_types3);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        tabLayout = view.findViewById(R.id.tab_shop);
        linearLayout = view.findViewById(R.id.layout_shop);
        cardView = view.findViewById(R.id.cv_shop);

        linearLayout.setVisibility(View.GONE);
        cardView.setVisibility(View.GONE);
        tabLayout.addTab(tabLayout.newTab().setText("BROWSE"));

        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));

        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

    }


    private void readCompanyTypeRecords() {

        images.add("all");
        images.add("restaurant");
        images.add( "retail");
        images.add("gadgets");
        images.add("foods");
        images.add("services");
        images.add( "hotel");
        images.add("default_item");
        images.add( "shopping");

        cardView.setVisibility(View.VISIBLE);
        rcCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rcCategory.setHasFixedSize(true);
        companyTypeAdapter = new CompanyTypeAdapter(context, ShopFragment.this, companyTypeArrayList, images);
        companyTypeAdapter.setClickListener(this);
        rcCategory.setAdapter(companyTypeAdapter);
        registerForContextMenu(rcCompanyList);
    }

    private void clickItem1(ArrayList<Company> companyArrayList){

        linearLayout.setVisibility(View.VISIBLE);
        rcCompanyList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rcCompanyList.setHasFixedSize(true);
        companyListAdapter1 = new CompanyListAdapter(context, companyArrayList);
        companyListAdapter1.setClickListener(this);
        rcCompanyList.setAdapter(companyListAdapter1);
        registerForContextMenu(rcCompanyList);

    }

    private void clickItem2(ArrayList<Company> companyArrayList){

        rcCompanyTyp2.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rcCompanyTyp2.setHasFixedSize(true);
        companyListAdapter2 = new CompanyListAdapter2(context, companyArrayList);
        companyListAdapter2.setClickListener(this);
        rcCompanyTyp2.setAdapter(companyListAdapter2);
        registerForContextMenu(rcCompanyTyp2);
    }

    private void clickItem3(ArrayList<Company> companyArrayList){

        rcCompanyTyp3.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rcCompanyTyp3.setHasFixedSize(true);
        companyListAdapter3 = new CompanyListAdapter3(context, companyArrayList);
        companyListAdapter3.setClickListener(this);
        rcCompanyTyp3.setAdapter(companyListAdapter3);
        registerForContextMenu(rcCompanyTyp3);
    }



    private void readStoreList() {
        tvCompanyTypes1.setText("Restaurant");
        tvCompanyTypes2.setText("Shopping");
        tvCompanyTypes3.setText("Services");
    }

    // Online functions
    public void fetchCompanyTypes() {
        try {
            HttpClientProvider.get(context, "read_company_types/", null, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    try {

                        companyTypeArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<CompanyType>>(){}.getType());

                        CompanyType companyType = new CompanyType();
                        companyType.setName("All");
                        companyType.setUuid("");
                        companyTypeArrayList.add(0, companyType);

                        readCompanyTypeRecords();
                        fetchCompanyList1();

                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toasty.warning(context, "Can't Connect to Server").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.warning(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                }

            });


        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
        }
    }

    // Online functions
    public void fetchCompanyList1()

    {
        try
        {
             String uuid = "";
             int index = 0;

            for(CompanyType companyType : companyTypeArrayList){
                index++;
                if(index == 2){
                    uuid = companyType.getUuid();
                }
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("company_type", uuid);

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "read_store_list/", stringEntity, new JsonHttpResponseHandler()
            {

                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                         ArrayList<Company> companyArrayList;

                        companyArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Company>>(){}.getType());

                        clickItem1(companyArrayList);

                        fetchCompanyList2();

                    } catch (Exception err)
                    {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toasty.warning(context, "Network is unreachable").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.warning(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                    swipeLayout.setRefreshing(true);
                }

            });


        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }
//     Online functions
    public void fetchCompanyList2()
    {
        try
        {
            String uuid = "";
            int index = 0;

            for(CompanyType companyType : companyTypeArrayList){
                index++;
                if(index == 9){
                    uuid = companyType.getUuid();
                }
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("company_type", uuid);

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "read_store_list/", stringEntity, new JsonHttpResponseHandler()
            {

                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        ArrayList<Company> companyArrayList;
                        companyArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Company>>(){}.getType());
                        clickItem2(companyArrayList);
                        fetchCompanyList3();

                    } catch (Exception err)
                    {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toasty.warning(context, "Network is unreachable").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.warning(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                    swipeLayout.setRefreshing(true);
                }

            });


        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }
    // Online functions
    public void fetchCompanyList3()
    {
        try
        {
            String uuid = "";
            int index = 0;

            for(CompanyType companyType : companyTypeArrayList){
                index++;
                if(index == 6){
                    uuid = companyType.getUuid();
                }
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("company_type", uuid);

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpClientProvider.post(context, "read_store_list/", stringEntity, new JsonHttpResponseHandler()
            {
                @Override
                public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                    super.onPreProcessResponse(instance, response);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        ArrayList<Company> companyArrayList;
                        companyArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<Company>>(){}.getType());

                        clickItem3(companyArrayList);
                        readStoreList();
                    } catch (Exception err)
                    {
                        Toasty.error(context, err.toString()).show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toasty.warning(context, "Network is unreachable").show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toasty.warning(context, responseString).show();
                    swipeLayout.setRefreshing(false);
                }

                @Override
                public void onRetry(int retryNo) {
                    super.onRetry(retryNo);
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    swipeLayout.setRefreshing(false);
                }
            });

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    @Override
    public void onRefresh() {
        fetchCompanyTypes();
    }

    @Override
    public void onItemCompanyTypeClick(View view, int position) {

        selectedCompanyType = companyTypeAdapter.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putParcelable("COMPANYTYPE", selectedCompanyType);
        bundle.putBoolean("FROM_SHOP_FRAGMENT", false);

        Intent intent = new Intent();
        intent.setClass(context, StoreAndItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onItemClick(View view, int position) {

        selectedCompany = companyListAdapter1.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putParcelable("COMPANY", selectedCompany);
        bundle.putBoolean("FROM_SHOP_FRAGMENT", true);

        Intent intent = new Intent();
        intent.setClass(context, StoreAndItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onItemClick2(View view, int position) {

        selectedCompany = companyListAdapter2.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putParcelable("COMPANY", selectedCompany);
        bundle.putBoolean("FROM_SHOP_FRAGMENT", true);

        Intent intent = new Intent();
        intent.setClass(context, StoreAndItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onItemClick3(View view, int position) {

        selectedCompany = companyListAdapter3.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putParcelable("COMPANY", selectedCompany);
        bundle.putBoolean("FROM_SHOP_FRAGMENT", true);

        Intent intent = new Intent();
        intent.setClass(context, StoreAndItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    //Hide item in Action bar
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_cart);
        MenuItem quantityItem = menu.findItem(R.id.action_quantity);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        MenuItem menuRemove = menu.findItem(R.id.action_remove_all);
        menuRemove.setVisible(false);
        menuItem.setVisible(false);
        quantityItem.setVisible(false);
        searchViewItem.setVisible(false);
    }

}
