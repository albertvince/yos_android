package com.example.vien.yahshua_ordering_system.Model;

import com.google.gson.annotations.SerializedName;

public class Model {

    @SerializedName("uuid")
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
