package com.example.vien.yahshua_ordering_system.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.vien.yahshua_ordering_system.Classes.HttpClientProvider;
import com.google.gson.annotations.SerializedName;

public class Store implements Parcelable {

    @SerializedName("company_name")
    private String companyName;
    @SerializedName("avatar")
    private String itemImage;
    @SerializedName("uuid")
    private String uuId;

    public Store(){

    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public String getItemImage() {
        return itemImage;
    }

    public String getImageURL(){
        return HttpClientProvider.getBaseURL() + getItemImage();
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    protected Store(Parcel in) {
        companyName = in.readString();
    }

    public static final Creator<Store> CREATOR = new Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(companyName);
    }
}
